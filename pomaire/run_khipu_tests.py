# !/usr/bin/env python
import sys
from os.path import dirname, abspath

import django
from django.conf import settings

settings.configure(
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'pomaire',
            'USER': 'multifiber',
            'PASSWORD': 'multifiber',
            'HOST': 'localhost',
            'PORT': '',
        }
    },
    ROOT_URLCONF='khipu.urls',
    INSTALLED_APPS=[
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'khipu',
    ],
    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ],
    LOGGER_KHIPU='logger_khipu',
    USE_TZ=True,
    KHIPU_RECEIVER_ID='148653'.encode('utf-8'),
    KHIPU_SECRET_KEY='73ebf4fc9d41f9892ab00a12d5070cdf389767f6'.encode('utf-8'), # En bytes para hash
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]
)

try:
    # Django < 1.8
    from django.test.simple import DjangoTestSuiteRunner
except ImportError:
    # Django >= 1.8
    from django.test.runner import DiscoverRunner as DjangoTestSuiteRunner


def runtests():
    django.setup()
    parent = dirname(abspath(__file__))
    sys.path.insert(0, parent)
    test_runner = DjangoTestSuiteRunner(verbosity=1)
    failures = test_runner.run_tests(['khipu'], verbosity=1, interactive=True)
    if failures:
        sys.exit(failures)


if __name__ == '__main__':
    runtests()
