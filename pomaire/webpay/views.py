from os.path import join
import requests
import uuid
from uuid import UUID

from common.models import *

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from django.http import HttpResponse, HttpRequest
from django.template import RequestContext
from rest_framework.decorators import action
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from rest_framework import viewsets, status
from rest_framework.response import Response
from django.urls import reverse
from django.conf import settings

from .models import Payment, VD, VN, VC, SI, S2, NC, VP
from .serializers import PaymentSerializer

# Weasy
from django.template.loader import get_template
from django.template import RequestContext
from django.template.loader import render_to_string
from weasyprint import HTML, CSS
import tempfile

# Transbank SDK
from tbk.services import WebpayService
from tbk.commerce import Commerce
from tbk import INTEGRACION
from tbk import PRODUCCION
import string
import random

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()

class PaymentViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Webpay Payment
    retrieve:
        End point for Webpay Payment
    update:
        End point for Webpay Payment
    delete:
        End point for Webpay Payment
    """
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = (AllowAny,)


class TransactionViewSet(viewsets.ViewSet):
    """ Viewset para obtener la información de la transacción por webpay """

    def create_order_id(self, size=26, chars=string.ascii_letters + string.digits + '|_=&%.,~:/?[+!@()>-'):
        """ Crea una orden de compra y comprueba que sea única en el sistema """
        unique = False
        while(not unique):
            order_id = ''.join(random.SystemRandom().choice(chars) for _ in range(size))
            try:
                if Payment.objects.get(order_id=order_id).exists():
                    continue
            except Payment.DoesNotExist:
                unique = True
        return order_id

    def create(self, request, *args, **kwargs):
        """ POST recibe la información para crear la transacción
            Retorna orden de compra, url y token webpay
        """
        x = uuid.UUID(request.session["uuid"])
        info=InfoMatrix.objects.get(uuid_session=x)
        amount = info.amount
        buy_order = self.create_order_id()
        view = TransactionViewSet()
        return_url = request.build_absolute_uri(reverse('transition'))
        final_url = request.build_absolute_uri(reverse('comprobante'))

        host = HttpRequest.get_host(request)
        context = {}
        
        if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or ('localhost' in host) or ('127.0.0.1' in host):
            commerce_code = global_preferences['webpay_banda_commerce_code']
            key_data = settings.BANDA_KEY_DATA
            cert_data= settings.BANDA_CERT_DATA
            tbk_cert_data = settings.BANDA_TBK_CERT_DATA
        else:
            commerce_code = global_preferences['webpay_giga_commerce_code']
            key_data = settings.GIGA_KEY_DATA
            cert_data= settings.GIGA_CERT_DATA
            tbk_cert_data = settings.GIGA_TBK_CERT_DATA

        try:
            # Inicializar transaccion
            commerce = Commerce(commerce_code,key_data, cert_data, tbk_cert_data, settings.TBK_ENV)
            webpay = WebpayService(commerce)
            transaction = webpay.init_transaction(amount, buy_order, return_url, final_url)

            # Retornar url y token
            info = {
                "trans_token": transaction['token'],
                "trans_url": transaction['url'],
                "order": buy_order
            }

        except Exception as e:
            print(f"\n\nError inicializando transancción:\n{e}\n\n")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        return Response(info, status=status.HTTP_201_CREATED)


@csrf_exempt
def transition(request):
    """ Vista que maneja la página de transición de Webpay """
    host = HttpRequest.get_host(request)
    if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or ('localhost' in host) or ('127.0.0.1' in host):
        template_name='transition_bandaancha.html'
        commerce_code = global_preferences['webpay_banda_commerce_code']
        key_data = settings.BANDA_KEY_DATA
        cert_data= settings.BANDA_CERT_DATA
        tbk_cert_data = settings.BANDA_TBK_CERT_DATA
    else:
        template_name='transition_bandaancha.html'
        commerce_code = global_preferences['webpay_giga_commerce_code']
        key_data = settings.GIGA_KEY_DATA
        cert_data= settings.GIGA_CERT_DATA
        tbk_cert_data = settings.GIGA_TBK_CERT_DATA

    context = {}
    
    if request.method == 'POST':
        token_ws = request.POST.get('token_ws')
        commerce = Commerce(commerce_code,key_data, cert_data, tbk_cert_data, settings.TBK_ENV)
        webpay = WebpayService(commerce)
        result = webpay.get_transaction_result(token_ws)
        response_code = result['detailOutput'][0]['responseCode']
        # print(result)
        if response_code == 0:
            date = result['transactionDate'].strftime("%Y-%m-%dT%H:%M:%S")
            # Select correct payment type
            raw_payment_type = result['detailOutput'][0]['paymentTypeCode']
            payment_type = 0
            if raw_payment_type=="VD":
                payment_type = VD
            elif raw_payment_type=="VN":
                payment_type = VN
            elif raw_payment_type=="VC":
                payment_type = VC
            elif raw_payment_type=="SI":
                payment_type = SI
            elif raw_payment_type=="S2":
                payment_type = S2
            elif raw_payment_type=="NC":
                payment_type = NC
            elif raw_payment_type=="VP":
                payment_type = VP
            if result['detailOutput'][0]['commerceCode']!=str(commerce_code):
                context = {
                    "error": "Ha ocurrido un error con su pago, por favor inténtelo de nuevo más tarde."
                }
            else:
                context = {
                    "token_ws": token_ws,
                    "url": result['urlRedirection'],
                    "amount": result['detailOutput'][0]['amount'],
                    "buyOrder": result['detailOutput'][0]['buyOrder'],
                    "authorizationCode": result['detailOutput'][0]['authorizationCode'],
                    "sharesNumber": result['detailOutput'][0]['sharesNumber'],
                    "sharesAmount": result['detailOutput'][0]['sharesAmount'],
                    "transactionDate": date,
                    "cardNumber": result["cardDetail"]["cardNumber"],
                    "type": payment_type
                }
                webpay.acknowledge_transaction(token_ws)
        elif response_code == -1:
            context = {
                # "error": "Su pago ha sido anulado, por favor inténtelo de nuevo más tarde."
                "error": "Las posibles causas de este rechazo son:\n-Error en el ingreso de los datos de su tarjeta de Crédito o Débito (fecha y/o código de seguridad).\n-Su tarjeta de Crédito o Débito no cuenta con saldo suficiente.\n-Tarjeta aún no habilitada en el sistema financiero.",
                "buyOrder": result['detailOutput'][0]['buyOrder']
                }
        elif response_code == -3 or response_code == -4:
            context = {
                # "error": "Su pago ha sido rechazado, por favor inténtelo de nuevo más tarde."
                "error": "Las posibles causas de este rechazo son:\n-Error en el ingreso de los datos de su tarjeta de Crédito o Débito (fecha y/o código de seguridad).\n-Su tarjeta de Crédito o Débito no cuenta con saldo suficiente.\n-Tarjeta aún no habilitada en el sistema financiero.",
                "buyOrder": result['detailOutput'][0]['buyOrder']
            }
        else:
            context = {
                "error": "Ha ocurrido un error con su pago, por favor inténtelo de nuevo más tarde."
            }
            # return Response(info, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # print(context)
    return render(request, template_name, context)

## Información que retorna webpay.get_transaction_result(token_ws)
# OrderedDict([
    # ('accountingDate', '1211'), 
    # ('buyOrder', 'pxtrTpa29ijq/!yv7,-7Mz->W8'), 
    # ('cardDetail', OrderedDict([('cardNumber', '6623'), 
    #                 ('cardExpirationDate', None)])), 
    # ('detailOutput', [OrderedDict([('sharesAmount', None), 
    #                     ('sharesNumber', 0), 
    #                     ('amount', Decimal('6200')), 
    #                     ('commerceCode', '597020000540'), 
    #                     ('buyOrder', 'pxtrTpa29ijq/!yv7,-7Mz->W8'), 
    #                     ('authorizationCode', '1213'), 
    #                     ('paymentTypeCode', 'VN'), 
    #                     ('responseCode', 0)])]),
    # ('sessionId', None),
    # ('transactionDate', datetime.datetime(2019, 12, 11, 13, 24, 17, 296000, tzinfo=<FixedOffset '-03:00'>)),
    # ('urlRedirection', 'https://webpay3gint.transbank.cl/webpayserver/voucher.cgi'), 
    # ('VCI', 'TSY')])