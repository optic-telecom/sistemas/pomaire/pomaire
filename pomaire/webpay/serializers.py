from rest_framework import serializers
from common.models import BasePayment, PayedInvoice
from .models import Payment
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from datetime import datetime
from decimal import *
from django.conf import settings

class PaymentSerializer(serializers.ModelSerializer):
    """ Serializer de pagos """
    customer_rut = serializers.CharField(write_only=True, max_length=20)
    amount = serializers.DecimalField(write_only=True, max_digits=32, decimal_places=2)
    invoices = serializers.CharField(write_only=True)

    class Meta:
        model = Payment
        fields = ('amount', 'customer_rut', 'invoices', 'transaction_date',
            'order_id', 'authorization_code', 'shares_number', 'shares_amount',
            'payment_type', 'last_card_number')

    def create(self, validated_data):
        """ Crea automáticamente el pago base. """
        #print(validated_data)

        # To get the pay ammount 
        if 'shares_amount' in validated_data:
            money=int(validated_data["shares_amount"])
        else:
            money=int(validated_data["amount"])

        # Info que se enviara a matrix 
        data_to_matrix={
            'paid_on': validated_data["transaction_date"],
            'deposited_on': validated_data["transaction_date"],
            'amount':money,
            'manual':False,
            'kind': 8,
            'operation_number': validated_data["authorization_code"], 
            'transfer_id': validated_data["order_id"], 
            'cleared': False,
            'comment': "Pago desde Pomaire cliente "+ str(validated_data["customer_rut"]),
            'customer_rut': validated_data["customer_rut"],
            'invoices': validated_data["invoices"],
        }
        # print(data_to_matrix)
        # print("Webpay")
        customer_rut = validated_data.pop('customer_rut')
        amount = validated_data.pop('amount')
        invoices = validated_data.pop('invoices')
        invoices_list = invoices.split(",")

        if global_preferences["allow_registry_in_matrix"]:
            response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
            #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
            print(response)
            if response.status_code == 201:
                # Crea el pago base y se marca como registrado en matrix.
                base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount, registered_in_matrix=True)
            else:
                # Crea el pago base y se marca como no registrado en matrix.
                base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount)
        else:
            # Crea el pago base.
            base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount)
        # Crear invoices pagados
        for folio in invoices_list:
            invoice = PayedInvoice.objects.create(invoice_id=folio, payment=base_payment)
        # Agrega pago base a validated_data
        validated_data['parent_payment'] = base_payment
        paypal_payment = Payment.objects.create(**validated_data)
        return paypal_payment

def otro():
    data_to_matrix={'paid_on': datetime.now(), 
    'deposited_on': datetime.now(), 
    'amount': 19980, 'manual': False, 'kind': 8, 
    'operation_number': '001123', 
    'transfer_id': '!n4=)4k&ygC7Q!BE=Au3Ye5U:t', 'cleared': False,
    'comment': 'Pago desde Pomaire cliente 6.616.979-0', 
    'customer_rut': '6.616.979-0', 'invoices': '11855,14309'}
    #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
    response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
    print(response)
    if response.status_code == 201:
        print("Se creo")
    else:
        print("No se creo")
    
#otro()