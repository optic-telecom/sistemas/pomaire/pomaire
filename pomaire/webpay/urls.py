from rest_framework import routers
from django.urls import path
from django.conf.urls import url

from .views import *

router = routers.DefaultRouter()

router.register(r'webpay/payment', PaymentViewSet, 'webpaypayment_api')
router.register(r'webpay/transaction', TransactionViewSet, 'webpaytransaction_api')

urlpatterns = [
    path('webpay/confirmacion/', transition, name="transition"),
]