from django.db import models
from common.models import BaseModel, BasePayment

# class CardType(BaseModel):
#     """ Información del tipo de tarjeta """
#     name = models.CharField(max_length=30, unique=True)
    
#     class Meta:
#         ordering = ['name']
    
#     def __str__(self):
#         return self.name


# class Card(BaseModel):
#     """ Información de la tarjeta """
#     number = models.CharField(max_length=16, unique=True)
#     card_type = models.ForeignKey(CardType, on_delete=models.SET_NULL, null=True, blank=True)
#     expiration_date = models.DateField(null=True, blank=True)
    
#     class Meta:
#         ordering = ['number']
    
#     def __str__(self):
#         return self.number


UNAUTHENTICATED = 0
TSY = 1
TSN = 2
TO = 3
ABO = 4
U3 = 5
NP = 6
ACS2 = 7
A = 8
INV = 9
EOP = 10
VCI_CHOICES = (
    (UNAUTHENTICATED, 'No se autenticó'),
    (TSY, 'Autenticación exitosa'),
    (TSN, 'Autenticación fallida'),
    (TO, 'Tiempo máximo excedido para autenticación'),
    (ABO, 'Autenticación abortada por tarjetahabiente'),
    (U3, 'Error interno en la autenticación'),
    (NP, 'No participa'),
    (ACS2, 'Autenticación fallida extranjera'),
    (A, 'Intento de autenticación'),
    (INV, 'Autenticación inválida'),
    (EOP, 'Error Operativo'),
)

VD = 1
VN = 2
VC = 3
SI = 4
S2 = 5
NC = 6
VP = 7
PAYMENT_TYPE_CHOICES = (
    (VD, 'Venta Débito'),
    (VN, 'Venta Normal'),
    (VC, 'Venta en cuotas'),
    (SI, '3 cuotas sin interés'),
    (S2, '2 cuotas sin interés'),
    (NC, 'N Cuotas sin interés'),
    (VP, 'Venta Prepago'),
)

class Payment(BaseModel):
    """ Información de los pagos de Webpay Plus (Webpay Normal) """
    order_id = models.CharField('Orden de compra único', max_length=26, unique=True)
    parent_payment = models.OneToOneField(BasePayment, on_delete=models.PROTECT, related_name='general_webpay_payment')
    # session_id = models.CharField('Identificador único de sesión', max_length=61, unique=True, null=True, blank=True)
    transaction_date = models.DateTimeField('Fecha y hora de autorización')
    # verification = models.PositiveSmallIntegerField(choices=VCI_CHOICES, null=True, blank=True) # Por ahora no me parece guardarlo
    authorization_code = models.CharField('Código de autorización de la transacción', max_length=6)
    payment_type = models.PositiveSmallIntegerField(choices=PAYMENT_TYPE_CHOICES, null=True, blank=True)
    shares_number = models.PositiveSmallIntegerField('Cantidad de cuotas', null=True, blank=True) # Por si se realiza el pago en cuotas
    shares_amount = models.DecimalField('Monto de cada cuota', max_digits=32, decimal_places=4, null=True, blank=True)
    last_card_number = models.CharField(max_length=4, null=True, blank=True)

    class Meta:
        ordering = ['-id']
    