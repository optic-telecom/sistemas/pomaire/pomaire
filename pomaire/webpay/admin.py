from django.contrib import admin
from common.admin import BaseAdmin
from import_export.admin import ImportExportModelAdmin
from .models import *

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()

# @admin.register(CardType)
# class CardTypeAdmin(BaseAdmin):
#     list_display = ('id','name')


# @admin.register(Card)
# class CardAdmin(BaseAdmin):
#     list_display = ('id','number','card_type','expiration_date')


@admin.register(Payment)
class PaymentAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id','transaction_date', 'authorization_code',
        'payment_type', 'shares_number', 'shares_amount', 'last_card_number')