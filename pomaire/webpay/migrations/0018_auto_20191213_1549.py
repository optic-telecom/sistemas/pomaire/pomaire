# Generated by Django 2.2.5 on 2019-12-13 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webpay', '0017_auto_20191213_1516'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalpayment',
            name='shares_amount',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=32, null=True, verbose_name='Monto de cada cuota'),
        ),
        migrations.AddField(
            model_name='payment',
            name='shares_amount',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=32, null=True, verbose_name='Monto de cada cuota'),
        ),
    ]
