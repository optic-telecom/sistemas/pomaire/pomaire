# Generated by Django 2.2.5 on 2019-09-23 18:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
        ('webpay', '0007_auto_20190916_1205'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalpayment',
            name='parent_payment',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='common.BasePayment'),
        ),
        migrations.AddField(
            model_name='payment',
            name='parent_payment',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='general_webpay_payment', to='common.BasePayment'),
        ),
    ]
