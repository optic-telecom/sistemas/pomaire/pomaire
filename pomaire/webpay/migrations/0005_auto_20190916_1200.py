# Generated by Django 2.2.5 on 2019-09-16 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webpay', '0004_auto_20190915_1742'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historicalpayment',
            options={'get_latest_by': 'history_date', 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical payment'},
        ),
        migrations.AlterModelOptions(
            name='payment',
            options={'ordering': ['buy_order']},
        ),
        migrations.AlterField(
            model_name='historicalpayment',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=32, verbose_name='Amount'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=32, verbose_name='Amount'),
        ),
    ]
