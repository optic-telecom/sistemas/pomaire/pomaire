$(document).ready(function(){
  
    $(document).prop('title', DjangoURL('title')+' - Comprobante de pago');
    // var cancelled = localStorage.getItem("cancelled");
    // if(cancelled==null){
    //     _error_redirect("No puede acceder a esta página sin haber realizado algún pago",Django.inicio);
    //     return;
    // }
    // localStorage.removeItem("cancelled");
    // $('#info').removeAttr("hidden");
    var rut = localStorage.getItem("rut");
    var debt = localStorage.getItem("debt");
    var invoices = localStorage.getItem("invoices").split(",");
    var debts = localStorage.getItem("debts").split(",");
    $("td#rut").html(rut);
    $("td#total").html("$"+formatDebt(debt));
    for (var i = 0; i < invoices.length; i++) {
        $("#detail tbody").append(`<tr><td>${invoices[i]}</td><td>$${formatDebt(debts[i])}</td></tr>`);
    }
    $("td#fecha").html(today());
});