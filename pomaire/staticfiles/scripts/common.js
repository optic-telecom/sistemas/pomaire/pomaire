function today(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  } 
  if (mm < 10) {
    mm = '0' + mm;
  } 
  return dd + '/' + mm + '/' + yyyy;
};

function formatRUT(rut){
  // n.nnn.nnn (7) o nn.nnn.nnn (8) sin digito verificador
  var rut_separated = rut.split("-");
  if (rut_separated.length>1){
    return rut_separated[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + '-' + rut_separated[1].toUpperCase();  
  }
  return rut_separated[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

function formatDebt(debt){
  var debt_separated;
  debt_separated = debt.toString().split(".");
  if (debt_separated.length>1){
    return debt_separated[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ',' + debt_separated[1];
  }
  return debt_separated[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

//* alerts sweetalert *//
function _info(textMessage) {
  swal({
      title: "",
      text: textMessage,
      icon: "info",
      // buttons: false,
  });
};

function _error(textMessage) {
  noti = swal({
      title: "Error",
      text: textMessage,
      icon: "error",
      buttons: false,
  });
  return noti
};

function _error_redirect(textMessage,redirect,title='Error') {
  swal({  
      title: title,
      text: textMessage,
      icon: "error",
      showCancelButton: false,
      confirmButtonColor: "#23c6c8",
      confirmButtonText: "Cerrar",
      closeOnConfirm: false
      })
  .then(function() {
      location.href = redirect;
  });
};

function _exito(textMessage) {
  swal({
  title: "Información",
    text: textMessage,
    icon: "success",
    buttons: false
  });
};

function _exito_redirect(textMessage,dire) {
  swal({
  title: "Información",
    text: textMessage,
    icon: "success",
    buttons: false
  }).then(function() {
    location.href = dire;
  });
};

function _exito_redirect_here(textMessage) {
  swal({
  title: "Información",
    text: textMessage,
    icon: "success",
    buttons: false
  }).then(function() {
    location.reload();
  });
};

function _confirma(textMessage, yes, not) {
swal({
    title: "Información",
    text: textMessage,
    icon: "warning",
    buttons: [
      'No',
      'Si'
    ],
    dangerMode: true,
  }).then(function(isConfirm) {
    if (isConfirm) {
      yes()
    } else {
      not()
    }
  })

}

function handleErrorAxios(error) {
  if (error.response) {
  
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    // console.log(error.response.data);

    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){});
      }else{
        _error("Por favor verifique los datos ingresados.");
      }
    }
    else if (error.response.status == 401)
    {
      handleUnauthorizedAxios();
    }
    else if (error.response.status == 404)
    {
      console.log("error 404 ", error.request.responseURL)
    }
    else if(error.response.status == 500){
        _error('Hubo un error en el sistema. Contacte al administrador.')
        .then(function() {});
    }        
    else{
      console.info("Response:");
      console.log(error.response);
      // console.log(error.response.headers);
    }

  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.info("Request");
    console.log(error.request);
    console.log(error)
  } else {
    // Something happened in setting up the request that triggered an Error
    // console.log('Error message: ', error.message);
    console.log('Error stack: ', error.stack);
    _error(error.message);
  }
  //console.log(error.config);
}

function handleUnauthorizedAxios() {
  _error_redirect("Se vencio su session, porfavor ingrese nuevamente",
  '/','Sesion expirada');
}

function handleTooManyRequestsAxios()
{
  console.log("Too many requests ");
}

function verifyJwtToken() {
  axios.post('/api/v1/auth/verify_jwt_token/', {token: HDD.get(Django.name_jwt)})
  .then(function(response){
    if (response.status != 200)
    {
      sessionExit(true);
    }
  })
  .catch(function(error) {
      sessionExit(true);
  });    
}

function DjangoURL(url, id='', version='v1') {
  return Django[url].replace(':val:',id).replace('v1',version);
}