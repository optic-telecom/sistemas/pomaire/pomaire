$(document).ready(function(){
  
    $(document).prop('title', DjangoURL('title')+' - Comprobante de pago');
    
    var cancelled = localStorage.getItem("cancelled");
    var init = localStorage.getItem("init_webpay");
    var method_pay = localStorage.getItem("payment_method");
    if(cancelled==null){
      if ($("#TBK_ORDEN_COMPRA").val()!="" && !(method_pay=="Paypal")){
        if(init!=null){
          localStorage.removeItem("init_webpay"); // Remove because we don't need it here anymore
        }
        _error_redirect("Su orden de compra ha sido anulada.",Django.inicio, "Orden de compra "+$("#TBK_ORDEN_COMPRA").val()+" anulada");
        return;
      }
      _error_redirect("No puede acceder a esta página sin haber realizado algún pago.",Django.inicio);
      return;
    }
    localStorage.removeItem("cancelled");

    $("#btn_reinicio").click(function(){
      reiniciar();
    });

    $('#info').removeAttr("hidden");
    var rut = Info.rut; //localStorage.getItem("rut");
    var amount = Info.amount; // localStorage.getItem("amount");
    var order = localStorage.getItem("order");
    var raw_invoice_no = Info.invoices; // localStorage.getItem("invoices");
    var invoices = raw_invoice_no.split(",");
    var raw_payed_invoices = localStorage.getItem("payed_invoices");
    var payed_invoices = raw_payed_invoices.split(",");
    var raw_debts = Info.debts; // localStorage.getItem("debts");
    var debts = raw_debts.split(",");
    var raw_descriptions = Info.descriptions; // localStorage.getItem("descriptions");
    var descriptions = raw_descriptions.split(",");
    var method = localStorage.getItem("payment_method");
    var date = today();
    var total = "$"+formatDebt(amount);
    
    $("td#rut").html(rut);
    $("dd#factura-id").html(order);
    $("dd#method").html(method);
    $("td#total").html(total);
    
    var raw_formatted_debts = "";
    for (var i = 0; i < payed_invoices.length; i++) {
      var index = parseInt(payed_invoices[i]);
      raw_formatted_debts += "$"+formatDebt(debts[index])+",";
      $("#detail tbody").append(`<tr><td>${invoices[index]}</td><td>${descriptions[index]}</td><td>$${formatDebt(debts[index])}</td></tr>`);
    }
    $("td#fecha").html(date);
    
    // Webpay info if necessary
    var auth_code = localStorage.getItem("authorization_code");
    if(auth_code!=null){
      $('#detail').css({"margin-top": "160px"});
      var payment_type = parseInt(localStorage.getItem("payment_type"));
      var type = "";
      if (payment_type==1){
        type = "Débito";
      }else if (payment_type==2){
        type = "Crédito";
      }else if (payment_type==7){
        type = "Prepago";
      }else{
        type = "Crédito en cuotas";
      }
      $('#payment_type_form').val(type);
      $('#auth_form').val(auth_code);
      $("#informations").append(`<dt>Código de autorización: </dt>`);
      $("#informations").append(`<dd>${auth_code}</dd>`);
      $("#informations").append(`<dt>Tipo de pago: </dt>`);
      $("#informations").append(`<dd>${type}</dd>`);
      var shares_number = localStorage.getItem("shares_number");
      if(shares_number!=null){
        var shares_amount = localStorage.getItem("shares_amount");
        $("#informations").append(`<dt>${shares_number} cuotas: </dt>`);
        $("#informations").append(`<dd>$${formatDebt(shares_amount)} c/u</dd>`);
        $('#shares_number_form').val(shares_number+ " cuotas: ");
        $('#shares_amount_form').val("$"+formatDebt(shares_amount) + " c/u");
      }else{
        $("#informations").append(`<dt>Sin cuotas</dt>`);
        $('#shares_number_form').val("Sin cuotas");
        $("#informations").append(`<dd></dd>`);
      }
      var card_no = localStorage.getItem("last_card_number");
      $("#informations").append(`<dt>Últimos dígitos de la tarjeta: </dt>`);
      $("#informations").append(`<dd>${card_no}</dd>`);
      $('#card_number_form').val(card_no);
    }

    // Add values to form
    $('#numero_form').val(order);
    $('#fecha_form').val(date);
    $('#rut_form').val(rut);
    $('#metodo_form').val(method);
    $('#total_form').val(total);
    $('#invoice_form').val(raw_invoice_no);
    $('#payed_invoices_form').val(raw_payed_invoices);
    $('#debts_form').val(raw_formatted_debts);
    $('#descriptions_form').val(raw_descriptions);

    $("#btn_download").click(function(){
      var url_action = Django.receipt;
      var form = $('#receipt_form').attr('action', url_action);
      $('#payed_form').val("trans_payed");
      form.submit();
    });

    $("#email-form").attr('onsubmit','return send_mail()');

});

function reiniciar(){
  localStorage.clear();
  location.href = Django.inicio;
};

function send_mail(){
  $('#text-spinner').text("Su recibo está siendo enviado, por favor no cierre ni refresque la página.");
  $('#modal-spinner').modal('show');
  var url = DjangoURL('send_email');
  var data = {
    "email": $("#email").val(),
    "numero": $('#numero_form').val(),
    "fecha": $('#fecha_form').val(),
    "rut": $('#rut_form').val(),
    "metodo": $('#metodo_form').val(),
    "total": $('#total_form').val(),
    "invoice_no": $('#invoice_form').val(),
    "payed_invoices": $('#payed_invoices_form').val(),
    "debts": $('#debts_form').val(),
    "descriptions": $('#descriptions_form').val()
  }
  // Check whether there is some webpay info to send webpay receipt mail
  var auth_code = $('#auth_form').val();
  if (auth_code!=""){
    data["auth_code"] = auth_code;
    data["payment_type"] = $('#payment_type_form').val();
    data["shares_number"] = $('#shares_number_form').val();
    data["shares_amount"] = $('#shares_amount_form').val();
    data["card_number"] = $('#card_number_form').val();
  }
  axios.post(url, data)
  .then(function(res){
    $('#modal-spinner').modal('hide');
    if (res.status == 201)
    {
      var error = res.data["error"]
      if (error){
        throw new Error(error);
      }
      _exito("Su recibo ha sido enviado.")
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
    $('#modal-spinner').modal('hide');
    handleErrorAxios(error);
  });
  return false;
}