
$(document).ready(function(){
  
  $(document).prop('title', 'Pomaire - Comprobando pago Webpay Plus');
  
  // error = "{{error}}"; TODO: ERROR HANDLING
  // if (error){
  //   console.log(error);
  //   throw new Error(error);
  // }
  var error = $('#error').val();
  if(error!=""){
    _error_redirect(error, Django.inicio, "Orden de compra "+$('#buyOrder').val()+" anulada");
    return;
  }

  var init = localStorage.getItem("init_webpay");
  if(init==null){
    _error_redirect("No puede acceder a esta página sin haber iniciado algún pago con Webpay Plus.",Django.inicio);
    return;
  }
  localStorage.removeItem("init_webpay");

  $('#text-spinner').text("Su pago está siendo procesado, por favor no cierre ni refresque la página.");
  $('#modal-spinner').modal('show');

  // Confirmation
  var amount = Info.amount_str; //localStorage.getItem("amount");
  if(amount.localeCompare($('#amount').val())!=0){
    var error_str = "Ha ocurrido un error con su pago, por favor inténtelo más tarde.";
    _error_redirect(error_str, Django.inicio,"Orden de compra "+$('#buyOrder').val()+" anulada");
    return;
  }

  var webpay_order = localStorage.getItem("order_webpay")
  if(webpay_order.localeCompare($('#buyOrder').val())!=0){
    var error_str = "Ha ocurrido un error con su pago, por favor inténtelo más tarde.";
    _error_redirect(error_str, Django.inicio, "Orden de compra "+$('#buyOrder').val()+" anulada");
    return;
  }
  localStorage.removeItem("webpay_order");
  localStorage.setItem("order", $('#buyOrder').val());

  // Buscar id de las facturas
  var invoices_folio = Info.invoices.split(","); // localStorage.getItem("invoices").split(",");
  var payed_invoices_list = localStorage.getItem("payed_invoices").split(",");
  var final_payed_invoices = []
  for (var i = 0; i < payed_invoices_list.length; i++){
    final_payed_invoices.push(invoices_folio[parseInt(payed_invoices_list[i])]);
  }

  // Save payment
  var url = DjangoURL('api_webpay_payment');
  var data = {
    "amount": amount,
    "customer_rut": Info.rut, //localStorage.getItem("rut"),
    "invoices": final_payed_invoices.toString(),
    "transaction_date": $('#transactionDate').val(),
    "order_id": localStorage.getItem("order"),
    "authorization_code": $('#authorizationCode').val(),
    "shares_number": $('#sharesNumber').val(),
    "last_card_number": $('#cardNumber').val(),
    "payment_type": $('#paymentType').val()
  }
  if (parseInt($('#sharesNumber').val())!=0){
    data["shares_amount"] = $('#sharesAmount').val();
    localStorage.setItem("shares_number", $('#sharesNumber').val());
    localStorage.setItem("shares_amount", $('#sharesAmount').val());
  }

  axios.post(url, data)
  .then(function(res){
    if (res.status == 201)
    {
      // Confirm
      localStorage.setItem("authorization_code", $('#authorizationCode').val());
      localStorage.setItem("last_card_number", $('#cardNumber').val());
      localStorage.setItem("payment_type", $('#paymentType').val());
      localStorage.setItem("cancelled", true);
      $('#modal-spinner').modal('hide');
      // Submit form
      $('#webpay_form').submit();
    }
    else {
      _error_redirect("Su pago no ha sido verificado, por favor comuníquese con personal técnico.", Django.inicio);
    }
  })//end then
  .catch(function(error) {
    $('#modal-spinner').modal('hide');
    _error_redirect("Su pago no ha sido verificado, por favor comuníquese con personal técnico.", Django.inicio);
    // handleErrorAxios(error); // Se cambio porque con status code 400 muestra "Por favor verifique los datos ingresados." que no tiene sentido en esta pagina
  });

});