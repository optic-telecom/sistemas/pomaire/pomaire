
// var conf = {
//   headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
// };

var numbers = ["1","2","3","4","5","6","7","8","9","0"];
var possible_values = ["1","2","3","4","5","6","7","8","9","0","k","K"];
var payed_invoices = [];

$(document).ready(function(){
  localStorage.clear();
  $(document).prop('title', DjangoURL('title'));

  detectPrivateMode(function (isPrivateMode) {
    // alert('is private mode: ' + isPrivateMode)
    if (isPrivateMode){
      $('#hidden-private').removeAttr("hidden");
    }
})

  $("#rut").attr('maxlength','12'); // Lo que se puede colocar inicialmente
  $("#rut").keyup(function(){
    
    var currentValue = $(this).val();
    currentValue = currentValue.replace("-","");
    currentValue = currentValue.replace(/\./g,"");
    $(this).val(formatRUT(currentValue)); // Replace after -
    var val_length = currentValue.length;
    // Check last typed character != -
    var last_char = currentValue.charAt(val_length-1)
    
    if (val_length > 7){ // Min n.nnn.nnn
      if (possible_values.indexOf(last_char)==-1){
        // It is not a number or k, erase
        $(this).val(formatRUT(currentValue.substring(0, val_length-1)));
        currentValue = currentValue.replace("-","");
        val_length = val_length - 1;
      }
      // If it passes verification, add "-"
      if(val_length>7){
        var penultimate = currentValue.charAt(val_length-2);
        if(val_length>8){
          // Check if k is in 8th position, with 9 digits
          if (numbers.indexOf(penultimate)==-1){
            $(this).val(formatRUT(currentValue.substring(0, val_length-2)));
            return;    
          }
        }
        var sub1 = formatRUT(currentValue.substring(0, val_length-1));
        var sub2 = currentValue.charAt(val_length-1);
        $(this).val(sub1+"-"+sub2);
      }
    }else{
      if (numbers.indexOf(last_char)==-1){
        // It is not a number, erase
        $(this).val(formatRUT(currentValue.substring(0, val_length-1)));
      }
    }
  });

  $("#btn_reinicio").click(function(){
    reiniciar();
  });

  $("#btn_webpay").click(function(){
    init_webpay_transaction();
  });

  $("#btn_nodebt").click(function(){
    reiniciar();
  });

  $("#ingresar-form").attr('onsubmit','return iniciar()');
  
  $("#btn_paypal").click(function(){
    select_paypal();
  });

  $("#btn_pay").click(function(){
    search = payed_invoices;
    var url = DjangoURL('ajax_monto');
    $.get(url+ search, function(json_response) {
      var amount = json_response.data['amount'];
      if(parseInt(amount)==0){
        _error("Debe seleccionar al menos una factura para pagar.");
      }else{
        localStorage.setItem("usd_debt", compute_usd_debt(amount));
        $('#payment-options').removeAttr("hidden");
        detectPrivateMode(function (isPrivateMode) {
          // alert('is private mode: ' + isPrivateMode)
          if (isPrivateMode){
            $('#hidden-webpay').prop("hidden",true);
          }
        })
        // Bloquear las facturas a pagar
        // $(":checkbox").prop("disabled", true);
        $(this).hide();
        $(document).scrollTop($(document).height()); 
      }
    });
  });

  function calculate_monto(){
    search = payed_invoices;
    var url = DjangoURL('ajax_monto');
    $.get(url+ search, function(json_response) {
      var amount = json_response.data['amount']
      alert(amount);
    });
    
  }

  function detectPrivateMode(cb) {
    var db,
    on = cb.bind(null, true),
    off = cb.bind(null, false)

    function tryls() {
        try {
            localStorage.length ? off() : (localStorage.x = 1, localStorage.removeItem("x"), off());
        } catch (e) {
            // Safari only enables cookie in private mode
            // if cookie is disabled then all client side storage is disabled
            // if all client side storage is disabled, then there is no point
            // in using private mode
            navigator.cookieEnabled ? on() : off();
        }
    }

    // Blink (chrome & opera)
    window.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on)
    // FF
    : "MozAppearance" in document.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off)
    // Safari
    : /constructor/i.test(window.HTMLElement) || window.safari ? tryls()
    // IE10+ & edge
    : !window.indexedDB && (window.PointerEvent || window.MSPointerEvent) ? on()
    // Rest
    : off()
}
  // $(document).on("change",":checkbox", function(){  // Dynamic
  $(document).on("click",":checkbox", function(){  // Dynamic
    var amount;
    if(this.id=="select-all"){
      var checked = $(this).prop('checked');
      $('#detail').find('input:checkbox').prop('checked', checked);
      payed_invoices = [];
      if(checked){
        search = payed_invoices;
        var url = DjangoURL('ajax_monto');
        $.get(url+ search, function(json_response) {
          var invoices = json_response.data["invoices"].split(",");
          for (var i = 0; i < invoices.length; i++) {
            payed_invoices.push(i);
          }
        });
      }else{
        amount = 0;
      }
    }else{
      //amount = localStorage.getItem("amount");
      var value = $(this).val().split(",");
      if(this.checked){
        // Add to amount
        //amount = parseInt(amount) + parseInt(value[0]);
        payed_invoices.push(value[1]);
      }else{
        $("#select-all").prop('checked', false);
        // Substract from amount
        //amount = parseInt(amount) - parseInt(value[0]);
        // Remove element
        for (var i = 0; i < payed_invoices.length; i++) {
          if (payed_invoices[i]==value[1]){
            payed_invoices.splice(i, 1);
            break;
          }
        }
      }
    }
    search=payed_invoices;
    var url = DjangoURL('set_info');
    $.get(url+ search, function(json_response) {
      var amount = json_response.data['amount'];
      $("td#total").html("$"+formatDebt(amount));
    });
  });

});

function iniciar(){
  $("#ingresar-form").removeAttr('onsubmit');
  $("#btn_inicio").prop('type', 'button');
  var url = DjangoURL('api_rut');
  var rut = formatRUT($("#rut").val());
  var data = {
    "rut": rut
  }
  axios.post(url, data)
  .then(function(res){
    if (res.status == 201)
    {
      var error = res.data["error"]
      if (error){
        throw new Error(error);
      }
      var debt = res.data["debt"];
      localStorage.setItem("rut", res.data["rut"]);
      $('#rut').prop("disabled", true);
      $("#rut").val(res.data["rut"]);
      $('#rut-label').hide();

      if(debt<1){
        // Mostrar solo deuda
        $('#hidden-nodebt-info').removeAttr("hidden");
        $('#hidden-private').attr("hidden", true);
        $('#btn_inicio').hide();
        $("#btn_nodebt").removeAttr("hidden");
        $('#rut-card').animate({height:"510px"}, "fast");
        return;
      }

      $(document).prop('title', DjangoURL('title'));
      $('#rut-card').animate({height:"350px"}, "fast");
      $('.container').animate({height:"75%"}, "fast");
      // $("#ingresar-form").removeAttr('onsubmit');
      $("#btn_inicio").prop('type', 'submit');
      $("#ingresar-form").attr('onsubmit','return reload_debts()');

      // var formatted_debt = formatDebt(debt);
      //localStorage.setItem("amount", 0);
      //localStorage.setItem("debt", debt);
      //localStorage.setItem("invoices", res.data["invoices"]);
      //localStorage.setItem("debts", res.data["debts"]);
      //localStorage.setItem("descriptions", res.data["descriptions"]);
      $("h3#customer_name").html(res.data["customer_name"]);
      $("#detail tbody").append(`<tr><td>Agregar/Remover boleta</td><td></td><td></td><td><button class="btn yellow_small_btn" id="btn_plus">+</button> <button class="btn yellow_small_btn" id="btn_minus">-</button></td></tr>`);
      // $("#detail tbody").append(`<tr><td>Seleccionar todas</td><td></td><td></td><td><input type="checkbox" id="select-all"/></td></tr>`);
      for (var i = 0; i < res.data["invoices"].length; i++) {
        $("#detail tbody").append(`<tr><td>${res.data["invoices"][i]}</td><td>${res.data["descriptions"][i]}</td><td>$${formatDebt(res.data["debts"][i])}</td><td><input type="checkbox" disabled value="${res.data["debts"][i]},${i}" id="check-${i}"/></td></tr>`);
      }
      $("td#total").html("$"+formatDebt(0));
      $('#btn_reinicio').removeAttr("hidden");
      $('#hidden-info').removeAttr("hidden");
      // $("#debt").text("$"+formatted_debt);
      $('#invoices').removeAttr("hidden");

      // Seleccionar el minimo de invoice
      var min_required =1;
      var expr = ":checkbox:lt("+min_required+")"
      $(expr).prop("checked", true);
      // $(expr).prop("disabled", true);
      // Add debts
      var debts = res.data["debts"];//.split(",");
      var amount = 0;
      for (var i = 0; i < min_required; i++) {
        payed_invoices.push(i);
        amount = amount + parseInt(debts[i]);
      }
      search = payed_invoices;
      var url = DjangoURL('set_info');
      $.get(url+ search, function(json_response) {
        var amount = json_response.data['amount']
        $("td#total").html("$"+formatDebt(amount));
        
        //select_required_invoices(3); // Esto es temporal
        $("#btn_plus").click(add_invoice);
        $("#btn_minus").click({min_required: 1}, remove_invoice);
      });
      $(document).scrollTop($(document).height()); 
    }
    else{
      $("#btn_inicio").prop('type', 'submit');
      $("#ingresar-form").attr('onsubmit','return iniciar()');
    }
  })//end then
  .catch(function(error) {
    $("#btn_inicio").prop('type', 'submit');
    $("#ingresar-form").attr('onsubmit','return iniciar()');
    handleErrorAxios(error);
  });

  return false;
}

function reload_debts(){
  $('#text-spinner').text("Sus deudas se están comprobando, por favor no cierre ni refresque la página.");
  //$('#modal-spinner').modal('show');
  var url = DjangoURL('api_rut');
  var rut = localStorage.getItem("rut");
  var data = {
    "rut": rut
  }
  axios.post(url, data)
  .then(function(res){
    //$('#modal-spinner').modal('hide');
    if (res.status == 201)
    {
      var error = res.data["error"]
      if (error){
        throw new Error(error);
      }
      var debt = res.data["debt"];
      // var formatted_debt = formatDebt(debt);
      //localStorage.setItem("debt", debt);
      //localStorage.setItem("invoices", res.data["invoices"]);
      //localStorage.setItem("debts", res.data["debts"]);
      //localStorage.setItem("descriptions", res.data["descriptions"]);
      //localStorage.setItem("amount", 0);
      $('tr', $("#detail tbody")).remove(); // Sentinel
      $("#detail tbody").append(`<tr><td>Agregar/Remover boleta</td><td></td><td></td><td><button class="btn yellow_small_btn" id="btn_plus">+</button> <button class="btn yellow_small_btn" id="btn_minus">-</button></td></tr>`);
      // $("#detail tbody").append(`<tr><td>Seleccionar todas</td><td></td><td></td><td><input type="checkbox" id="select-all"/></td></tr>`);
      for (var i = 0; i < res.data["invoices"].length; i++) {
        $("#detail tbody").append(`<tr><td>${res.data["invoices"][i]}</td><td>${res.data["descriptions"][i]}</td><td>$${formatDebt(res.data["debts"][i])}</td><td><input type="checkbox" disabled value=${res.data["debts"][i]} id="check-${i}"/></td></tr>`);
      }
      $("td#total").html("$"+formatDebt(0));
      var usd_debt = compute_usd_debt(debt);
      localStorage.setItem("usd_debt", usd_debt);
      payed_invoices=[]
      // Seleccionar el minimo de invoice
      var min_required =1;
      var expr = ":checkbox:lt("+min_required+")"
      $(expr).prop("checked", true);
      // $(expr).prop("disabled", true);
      // Add debts
      var debts = res.data["debts"];//.split(",");
      var amount = 0;
      for (var i = 0; i < min_required; i++) {
        payed_invoices.push(i);
        amount = amount + parseInt(debts[i]);
      }
      search = payed_invoices;
      var url = DjangoURL('set_info');
      $.get(url+ search, function(json_response) {
        amount = json_response.data['amount']
        $("td#total").html("$"+formatDebt(amount));
      });
      //select_required_invoices(3); // Esto es temporal
      $("#btn_plus").click(add_invoice);
      $("#btn_minus").click({min_required: 1}, remove_invoice);
      // $("#debt").text("$"+formatted_debt);
      _info("Su deuda ha sido comprobada.");
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios(); // Esto nunca va a pasar por ahora sin permisos
    }
  })//end then
  .catch(function(error) {
    $('#modal-spinner').modal('hide');
    handleErrorAxios(error);
  });
  return false;
}

function compute_usd_debt(debt){
  // Calculo de la deuda en dolares, referencia BCCL 03/10
  var exchange_rate = Django.exchange_rate;
  var usd_percentage = Django.usd_percentage;
  var usd_plus = Django.usd_plus;
  var pesos2usd = (debt/exchange_rate);
  var cal1=pesos2usd+usd_plus;
  var cal2=1-usd_percentage;
  var usd_debt = cal1/cal2;
  // var usd_debt = pesos2usd + pesos2usd*usd_percentage;
  usd_debt = Number.parseFloat(usd_debt).toFixed(2); // Redondear a dos decimales
  return usd_debt;
}

function select_required_invoices(min_required){
  var expr = ":checkbox:lt("+min_required+")"
  $(expr).prop("checked", true);
  // $(expr).prop("disabled", true);
  // Add debts
  var debts = localStorage.getItem("debts").split(",");
  var amount = 0;
  for (var i = 0; i < min_required; i++) {
    payed_invoices.push(i);
    amount = amount + parseInt(debts[i]);
  }
  localStorage.setItem("amount", amount);
  $("td#total").html("$"+formatDebt(amount));
}

function add_invoice(){
  search = payed_invoices;
  var url = DjangoURL('ajax_monto');
  $.get(url+ search, function(json_response) {
    //var amount = json_response.data['amount'];
    var invoices =  json_response.data['invoices'].split(",");
    var last = payed_invoices[payed_invoices.length-1]
    if (last!=invoices.length-1){
      var expr = ":checkbox:gt("+last+"):lt("+1+")";
      $(expr).prop("checked", true);
      payed_invoices.push(last+1);
      search = payed_invoices;
      var url = DjangoURL('set_info');
      $.get(url+ search, function(json_response) {
        var amount = json_response.data['amount'];
        $("td#total").html("$"+formatDebt(amount));
      });
    }else{
      _info("Ha seleccionado todas las boletas.");
    }
  });
}

function remove_invoice(event){
  if (payed_invoices.length>event.data.min_required){
    var last = payed_invoices.pop();
    var expr = ":checkbox:gt("+(last-1)+"):lt("+1+")";
    $(expr).prop("checked", false);
    //var debts = localStorage.getItem("debts").split(",");
    //var amount = localStorage.getItem("amount");
    //amount = parseInt(amount) - parseInt(debts[last]);
    //localStorage.setItem("amount", amount);
    search=payed_invoices;
    var url = DjangoURL('set_info');
    $.get(url+ search, function(json_response) {
      var amount = json_response.data['amount'];
      $("td#total").html("$"+formatDebt(amount));
    });
  }else{
    _info("No puede seguir eliminando boletas.");
  }
}

// function select_paypal(){
//   var amount = localStorage.getItem("amount");
//   var usd_debt = compute_usd_debt(amount);
//   localStorage.setItem("usd_debt", usd_debt);
//   // $('#payment-options').hide();
//   // $('#metodos-pago').removeAttr("hidden");
//   $(document).prop('title', 'Pomaire - Pagar usando Paypal');
//   $('#payment-options').animate({height:"70%"}, "fast");
//   return false;
// };

function init_webpay_transaction(){
  var url = DjangoURL('ajax_monto');
  search = payed_invoices;
  var amount;
  $.get(url+ search, function(json_response) {
      amount = json_response.data['amount'];
  });
  var url = DjangoURL('api_webpay_transaction');
  var data = {
    "amount": amount
  }
  axios.post(url, data)
  .then(function(res){
    if (res.status == 201)
    {
      var error = res.data["error"]
      if (error){
        throw new Error(error);
      }
      $('#token_form').val(res.data["trans_token"]);
      var form = $('#webpay_form').attr('action', res.data["trans_url"]);
      localStorage.setItem("payed_invoices", payed_invoices);
      localStorage.setItem("payment_method", "Webpay Plus");
      localStorage.setItem("order_webpay", res.data["order"]);
      localStorage.setItem("init_webpay", true);
      form.submit();
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios(); // Esto nunca va a pasar por ahora sin permisos
    }
  })//end then
  .catch(function(error) {
    handleErrorAxios(error);
  });
  return false;
}

function reiniciar(){
  localStorage.clear();
  location.reload();
};

function set_usd(){
    search= payed_invoices;
    var url = DjangoURL('ajax_monto');
    $.get(url+ search, function(json_response) {
      var amount = json_response.data['amount'];
      localStorage.setItem("usd_debt", compute_usd_debt(amount));
    });
};

// Smart buttons de paypal
paypal.Buttons({
    createOrder: function(data, actions) {
    // Set up the transaction
    var usd_debt = localStorage.getItem("usd_debt");
    return actions.order.create({
      purchase_units: [{
        amount: {
            value: usd_debt
        }
      }]
    });
  },
  onClick: function(data, actions) {
    set_usd();
  },
  onApprove: function(data, actions) {
    $('#text-spinner').text("Su pago está siendo procesado, por favor no cierre ni refresque la página.");
    $('#modal-spinner').modal('show');
    search = payed_invoices;
    var amount;
    var rut;
    var invoices;
      var url = DjangoURL('ajax_monto');
      $.get(url+ search, function(json_response) {
        amount = json_response.data['amount']
        rut = json_response.data["rut"];
        invoices = json_response.data["invoices"];
      });
    // Capture the funds from the transaction
    return actions.order.capture().then(function(details) {
      // alert(details.purchase_units[0].payments.captures[0].id);
      // console.log(details)
      // alert(details.purchase_units[0].payments.captures[0].seller_receivable_breakdown.net_amount.value);
      // Show a success message to your buyer
      // alert('Transaction completed by ' + details.payer.name.given_name);
      // Call your server to save the transaction
      // Encontrar los nro folio de matrix de facturas pagadas y enviar por invoices
      var invoices_folio = invoices.split(",");
      var final_payed_invoices = []
      var trans_id=details.purchase_units[0].payments.captures[0].id;
      for (var i = 0; i < payed_invoices.length; i++){
        final_payed_invoices.push(invoices_folio[parseInt(payed_invoices[i])]);
      }
      var url = DjangoURL('api_paypal_payment');
      return fetch(url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({
          'order_id': data.orderID,
          'amount': amount,
          'usd_amount': details.purchase_units[0].amount.value, // Paypal API
          'customer_rut': rut,
          'invoices': final_payed_invoices.toString(),
          'exchange_rate': Django.exchange_rate,
          'percentage': Django.usd_percentage,
          'usd_plus': Django.usd_plus,
          'transaction_date': details.create_time, 
          'transaction_id': trans_id
        })
      }).then(function(res) {
        $('#modal-spinner').modal('hide');
        if (res.status!=201){
          _error_redirect("Su pago no ha sido registrado, por favor comuníquese con personal técnico.", Django.inicio);
        }else{
          localStorage.setItem("cancelled", true);
          localStorage.setItem("payed_invoices", payed_invoices);
          localStorage.setItem("order", trans_id);
          localStorage.setItem("payment_method", "Paypal");
          _exito_redirect("Se ha registrado su pago satisfactoriamente", Django.invoice);
        }
        return res.json();
      }).then(function(details) {
        if (details.error === 'INSTRUMENT_DECLINED') {
          return actions.restart();
        }
      });
    });
  },
  onCancel: function (data) {
    _info("Transacción cancelada.");
  },
  onError: function (err) {
    // Show an error page here, when an error occurs
    console.log(err);
    _error('Ha ocurrido un error con paypal, por favor intente más tarde.');
  }
  }).render('#paypal-button-container');