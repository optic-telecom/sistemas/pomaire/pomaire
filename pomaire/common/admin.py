from django.contrib import admin

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()
from import_export.admin import ImportExportModelAdmin
from .models import BasePayment, PayedInvoice, InfoMatrix, Indicators

class BaseAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    actions = ['change_activation','change_status']

    def change_activation(self, request, queryset):
        model = self.model._meta.verbose_name
        n = queryset.count()
        message = "%(count)d %(model)s cambiadas." % {"count": n ,"model":model}
        self.message_user(request, message, messages.SUCCESS)   
        for item in queryset:
            if item.status_field:
                item.status_field = False
            else:
                item.status_field = True
            item.save()
        return queryset
    change_activation.short_description = 'Cambia el estado de la activacion'


    def change_status(self, request, queryset):
        if hasattr(self.model,'status'):
            model = self.model._meta.verbose_name
            n = queryset.count()
            message = "%(count)d %(model)s cambiadas." % {"count": n ,"model":model}
            self.message_user(request, message, messages.SUCCESS)   
            for item in queryset:

                if item.status:
                    item.status = False
                else:
                    item.status = True
                item.save()
        return queryset
    change_status.short_description = 'Cambia estatus'


@admin.register(BasePayment)
class BasePaymentAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id','amount','customer_rut','registered_in_matrix')

@admin.register(PayedInvoice)
class PayedInvoiceAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id','invoice_id','payment')

@admin.register(InfoMatrix)
class InfoMatrixAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id','customer_rut','amount', 'invoices')

@admin.register(Indicators)
class IndicatorsAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('kind','value','created')
