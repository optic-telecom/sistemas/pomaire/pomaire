from dynamic_preferences.registries import global_preferences_registry
import requests, json

global_preferences = global_preferences_registry.manager()

def generate_request(url, params):
    system_token = global_preferences['system_token']
    response = requests.post(url,
            headers={'Authorization': system_token},
            data=params,
            verify=False
    )
    return response
