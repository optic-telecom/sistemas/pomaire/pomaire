import json
import requests
from os.path import join
import datetime
import uuid
from uuid import UUID

from dynamic_preferences.registries import global_preferences_registry
from django.views.generic import TemplateView, FormView
from rest_framework import viewsets, status
from rest_framework.response import Response
from django.http import HttpResponse, HttpRequest
from django.urls import reverse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

# Weasy
from django.template.loader import get_template
from django.template import RequestContext
from django.template.loader import render_to_string
from weasyprint import HTML, CSS
import tempfile

# Emails
from django.core.mail import EmailMessage
from django.conf import settings
from django.http import Http404

# Transbank SDK
from tbk.services import WebpayService
from tbk.commerce import Commerce
from tbk import INTEGRACION

from .models import *
from .serializers import *

from itertools import cycle

global_preferences = global_preferences_registry.manager()

class RUTViewSet(viewsets.ViewSet):
    """ Viewset para obtener la información del cliente (RUT y deudas) al iniciar """

    def digito_verificador(self, rut):
        """ Verifica el dígito final del RUT 
            Obtenido de: https://gist.github.com/rbonvall/464824
            Modificado para que rut sea un string
        """
        separated_rut = rut.split('-')
        rut_str_no = separated_rut[0].replace('.','')
        rut_no = int(rut_str_no)
        reversed_digits = map(int, reversed(str(rut_no)))
        factors = cycle(range(2, 8))
        s = sum(d * f for d, f in zip(reversed_digits, factors))
        return (-s) % 11 # Si el resultado es 10, el RUT es "raya k".

    def format_number(self, num):
        """ Format del número con puntos y comas """
        # Cambia los . por - (para identificar decimales)
        string_number = '{:,}'.format(num).replace('.','-')
        string_number = string_number.replace(',','.')
        string_number = string_number.replace('-',',')
        return string_number

    def create(self, request, *args, **kwargs):
        """ POST recibe el RUT que se busca en matrix con sus deudas
            Retorna RUT, deuda total e invoices sin pagar en una lista
        """
        rut = request.data.get('rut')
        # Verificar digito verificador
        calculated_digito_verificador = self.digito_verificador(rut)
        digito_final = rut[-1]
        if calculated_digito_verificador != 10:
            if calculated_digito_verificador!=int(digito_final):
                info = {
                'error': 'RUT inválido'
                }
                # return Response(info, status=status.HTTP_201_CREATED)
                return Response(info, status=status.HTTP_400_BAD_REQUEST)
        else:
            if digito_final!='K':
                info = {
                'error': 'RUT inválido'
                }
                # return Response(info, status=status.HTTP_201_CREATED)
                return Response(info, status=status.HTTP_400_BAD_REQUEST)

        url = settings.MATRIX_URL+ global_preferences['search_rut_url']+rut # global_preferences['matrix_url']+ global_preferences['search_rut_url']+rut
        print(url)
        system_token = global_preferences['system_token']
        rut_request = requests.get(url,
            headers={'Authorization': system_token},
            verify=False)
        if rut_request.status_code == 200:
            customers = rut_request.json()
            if customers['count']!=1:
                # Manda un error
                info = {
                    'error': 'RUT inválido'
                }
                return Response(info, status=status.HTTP_400_BAD_REQUEST)
            else:
                customer_url = customers['results'][0]['customer']
                # Obtener el id usando url
                customer_id = customer_url.split('/')[-2] # Porque se retorna ['http:', '', '190.113.247.198', 'api', 'v1', 'customers', '5277', ''] por el ultimo /
                # Buscar a ver si el Plan es con UF

                # Traer últimas 5 facturas/boletas y calcular deuda
                url = settings.MATRIX_URL +global_preferences['get_debt_url']+customer_id+global_preferences['limit_url_parameter']+global_preferences['not_paid_url_parameter']
                #global_preferences['matrix_url']+global_preferences['get_debt_url']+customer_id+global_preferences['limit_url_parameter']+global_preferences['not_paid_url_parameter']
                print(url)
                debt_request = requests.get(url,
                    headers={'Authorization': system_token},
                    verify=False)
                if debt_request.status_code==200:
                    # Para obtener el nombre del cliente.
                    url = settings.MATRIX_URL +global_preferences['get_customer_url']+rut #global_preferences['matrix_url']+global_preferences['get_customer_url']+rut
                    rut_request = requests.get(url,
                        headers={'Authorization': global_preferences['system_token']},
                        verify=False)
                    nombre=" "
                    if rut_request.status_code == 200:
                        customer = rut_request.json()
                        if customer['count']!=1:
                            # Manda un error
                            info = {
                                'error': 'RUT inválido'
                            }
                            return Response(info, status=status.HTTP_400_BAD_REQUEST)
                        else:
                            nombre = customer['results'][0]['name']
                    else:
                        pass
                    # Calculo de la deuda
                    invoices = debt_request.json()
                    debt = 0
                    invoices_pre = []
                    for invoice in invoices['results']:
                        # Filtrando que solo se muestren las boletas y facturas.
                        #if invoice["kind"]==1 or invoice["kind"]==2:
                        if invoice["kind"]=="Boleta" or invoice["kind"]=="Factura" or invoice["kind"]=="Nota de cobro":
                            # Para obtener los numeros de servicio.
                            servicio= " "
                            ser=""
                            for i in invoice["service"]:
                                if servicio==" ":
                                    servicio=" Servicio: #" + str(i["number"]) 
                                    ser=str(i["number"])
                                else:
                                    servicio=servicio + " #" + str(i["number"])
                                    ser=ser+"#"+str(i["number"])

                            invoices_pre.append({
                                "folio": invoice['folio'],
                                "total": invoice['total'],
                                "description": invoice['comment'] + str(servicio),
                                "due_date": invoice['due_date'],
                                "service": str(ser)
                            })
                            debt = debt + invoice['total']
                    data = {
                        'rut': rut,
                        'debt': debt
                        # 'debt': 0 # Prueba para cuando queramos ver si no hay deudas
                    }
                    serializer = RUTSerializer(data=data)
                    serializer.is_valid(raise_exception=True)
                    info = serializer.validated_data
                    #print(invoices_pre)
                    # Order by date asc (mayor a menor)
                    invoices = []
                    for i in range(0, len(invoices_pre)):
                        #print(invoices_pre[i]['due_date'][0:-6])
                        try:
                            date = datetime.datetime.strptime(invoices_pre[i]['due_date'][0:-6],"%Y-%m-%dT%H:%M:%S")
                        except:
                            c=invoices_pre[i]['due_date'][0:-6].split(".")
                            date1=str(c[0])
                            #print(date1)
                            date = datetime.datetime.strptime(invoices_pre[i]['due_date'][0:-6].split(".")[0],"%Y-%m-%dT%H:%M:%S")
                        add = False
                        for j in range(0, len(invoices)):
                            if date <= invoices[j]['due_date']:
                                invoices.insert(j, {
                                    'folio': invoices_pre[i]['folio'],
                                    'debt': invoices_pre[i]['total'],
                                    'description': invoices_pre[i]['description'],
                                    'due_date': date,
                                    'service': invoices_pre[i]['service']
                                })
                                add = True
                                break
                        
                        if not add:
                            invoices.append({
                                'folio': invoices_pre[i]['folio'],
                                'debt': invoices_pre[i]['total'],
                                'description': invoices_pre[i]['description'],
                                'due_date': date,
                                'service': invoices_pre[i]['service']
                            })
                    # print(invoices)
                    # Add in order to final arrays
                    invoices_folio = []
                    invoices_debt = []
                    invoices_descrip = []
                    invoices_service = []
                    for i in range(0, len(invoices)):
                        invoices_folio.append(invoices[i]['folio'])
                        invoices_debt.append(invoices[i]['debt'])
                        invoices_descrip.append(invoices[i]['description'])
                        invoices_service.append(invoices[i]['service'])
                    info['customer_name'] = nombre
                    info['invoices'] = invoices_folio
                    info['debts'] = invoices_debt
                    info['descriptions'] = invoices_descrip

                    valor=uuid.uuid4()
                    request.session["uuid"]= valor.hex
                    folios=""
                    for i in invoices_folio:
                        if folios=="":
                            folios=str(i)  
                        else:
                            folios=folios+","+ str(i)
                    descrip=""
                    for i in invoices_descrip:
                        if descrip=="":
                            descrip=str(i)  
                        else:
                            descrip=descrip+","+str(i)
                    debts=""
                    for i in invoices_debt:
                        if debts=="":
                            debts=str(i)  
                        else:
                            debts=debts+","+str(i)
                    
                    services=""
                    for i in invoices_service:
                        if services=="":
                            services=str(i)  
                        else:
                            services=services+","+str(i)

                    # Guardando info que se recibio de Matrix
                    InfoMatrix.objects.create(
                        uuid_session = valor,
                        customer_rut = rut,
                        amount = 0,
                        debt = debt,
                        invoices = folios,
                        description = descrip,
                        debts = debts,
                        customer_name=nombre,
                        services=services
                    )
                    
                else:
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else: # mensaje de error
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(info, status=status.HTTP_201_CREATED)


class BasePaymentViewSet(viewsets.ModelViewSet):
    """ Viewset para pagos realizados """
    queryset = BasePayment.objects.all()
    serializer_class = BasePaymentSerializer


class PayedInvoiceViewSet(viewsets.ModelViewSet):
    """ Viewset para las facturas de pagos completados """
    queryset = PayedInvoice.objects.all()
    serializer_class = PayedInvoiceSerializer


class InicioView(TemplateView):
    """ Vista para inicio de pomaire """

    def get(self, request, *args, **kwargs):
        host = HttpRequest.get_host(request)
        context = {}
        print(settings.WEBPAY)
        if settings.WEBPAY=="True":
            print("PASOOO")
            context["show_webpay"]=True
        else:
            context["show_webpay"]=False
        context["paypal_currency"]=global_preferences['paypal_currency']
        if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or  ('localhost' in host) or ('127.0.0.1' in host):
            context["client_id"]=global_preferences['paypal_banda_client_id']
            template_name='inicio_bandaancha.html'   
            context["title"]="Pagos Banda Ancha" 
        else:
            context["title"]="Pagos Giga" 
            context["client_id"]=global_preferences['paypal_giga_client_id']
            template_name='inicio.html'
        return render(request, template_name=template_name, context=context, status=status.HTTP_200_OK)


class InvoiceView(TemplateView):
    """ Vista para factura de pomaire """

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(InvoiceView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        host = HttpRequest.get_host(request)
        x = uuid.UUID(request.session["uuid"])
        info=InfoMatrix.objects.get(uuid_session=x)
        context = {
            "amount": info.amount, 
            'invoices': info.invoices, 
            'debts': info.debts,
            'debt':info.debt, 
            'rut': info.customer_rut
        }
        if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or  ('localhost' in host) or ('127.0.0.1' in host):
            template_name='comprobante_bandaancha.html'
            # template_name='factura_bandaancha.html' # ESTO SOLO ES PARA PRUEBA
        else:
            template_name='factura_bandaancha.html'
        return render(request, template_name=template_name, context=context,  status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        host = HttpRequest.get_host(request)
        context = {}
        
        # Check what happened with webpay payment
        tbk_orden = request.POST.get('TBK_ORDEN_COMPRA', None)
        if tbk_orden:
            context['TBK_ORDEN_COMPRA'] = tbk_orden
        if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or  ('localhost' in host) or ('127.0.0.1' in host):
            template_name='comprobante_bandaancha.html'
        else:
            template_name='factura_bandaancha.html'
        return render(request, template_name=template_name, context=context, status=status.HTTP_200_OK)


def generate_invoice_pdf(request, *args, **kwargs):
    """ Genera el PDF """
    # print(request.POST.get('payed'))
    if request.POST.get('payed')!="trans_payed":
        raise Http404("El recibo no existe")

    # Check if webpay payment
    auth_code = request.POST.get('auth_code', None)
    if auth_code!="":
        html_template = get_template('factura_bandaancha_context_webpay.html')
        css = CSS(join(settings.BASE_DIR, 'staticfiles/css/style_factura_bandaancha_webpay.css'))
    else:
        html_template = get_template('factura_bandaancha_context.html')
        css = CSS(join(settings.BASE_DIR, 'staticfiles/css/style_factura_bandaancha.css'))
    # Capturar facturas en el formato correcto
    invoices = request.POST.get('invoice_no').split(",")
    payed_invoices = request.POST.get('payed_invoices').split(",")
    debts = request.POST.get('debts').split(",")
    descriptions = request.POST.get('descriptions').split(",")
    facturas = []
    for i in range(len(payed_invoices)):
        facturas.append(
            {
                "number": invoices[int(payed_invoices[i])],
                "description": descriptions[int(payed_invoices[i])],
                "amount": debts[int(payed_invoices[i])]
            }
        )

    # Customer info
    rut = request.POST.get('rut')
    nombre = ""
    direccion = ""
    url = settings.MATRIX_URL +global_preferences['get_customer_url']+rut#global_preferences['matrix_url']+global_preferences['get_customer_url']+rut
    rut_request = requests.get(url,
        headers={'Authorization': global_preferences['system_token']},
        verify=False)
    if rut_request.status_code == 200:
        customer = rut_request.json()
        if customer['count']!=1:
            # Manda un error
            info = {
                'error': 'RUT inválido'
            }
            return Response(info, status=status.HTTP_400_BAD_REQUEST)
        else:
            nombre = customer['results'][0]['name']
            direccion = customer['results'][0]['composite_address']
    else: # mensaje de error
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    info = {
        "rut": rut,
        "numero": request.POST.get('numero'),
        "fecha": request.POST.get('fecha'),
        "metodo": request.POST.get('metodo'),
        "total": request.POST.get('total'),
        "facturas": facturas,
        "nombre": nombre,
        "direccion": direccion
    }
    # Add webpay info if necessary
    if auth_code!="":
        info['auth_code'] = auth_code
        info['tipo_pago'] = request.POST.get('payment_type')
        info['numero_cuotas'] = request.POST.get('shares_number')
        info['monto_cuotas'] = request.POST.get('shares_amount')
        info['tarjeta'] = request.POST.get('card_number')
    
    # Create pdf
    rendered_html = html_template.render(info).encode(encoding="UTF-8")
    html = HTML(string=rendered_html, base_url=request.build_absolute_uri())
    pdf = html.write_pdf(stylesheets=[css])
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'filename=factura.pdf'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(pdf)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    return response


class SendEmailViewSet(viewsets.ViewSet):
    """ Viewset para enviar el pdf del comprobante por correo electrónico """

    def create(self, request, *args, **kwargs):
        """ Genera el PDF y lo envía por correo """
        # Check if webpay payment
        auth_code = request.data.get('auth_code', None)
        if auth_code!=None:
            html_template = get_template('factura_bandaancha_context_webpay.html')
            css = CSS(join(settings.BASE_DIR, 'staticfiles/css/style_factura_bandaancha_webpay.css'))
        else:
            html_template = get_template('factura_bandaancha_context.html')
            css = CSS(join(settings.BASE_DIR, 'staticfiles/css/style_factura_bandaancha.css'))
        
        # Capturar facturas en el formato correcto
        invoices = request.data.get('invoice_no').split(",")
        payed_invoices = request.data.get('payed_invoices').split(",")
        debts = request.data.get('debts').split(",")
        descriptions = request.data.get('descriptions').split(",")
        facturas = []
        for i in range(len(payed_invoices)):
            facturas.append(
                {
                    "number": invoices[int(payed_invoices[i])],
                    "description": descriptions[int(payed_invoices[i])],
                    "amount": debts[int(payed_invoices[i])]
                }
            )

        # Customer info
        info={}
        rut = request.data.get('rut')
        nombre = ""
        direccion = ""
        url =  settings.MATRIX_URL +global_preferences['get_customer_url']+rut #global_preferences['matrix_url']+global_preferences['get_customer_url']+rut
        rut_request = requests.get(url,
            headers={'Authorization': global_preferences['system_token']},
            verify=False)
        if rut_request.status_code == 200:
            customer = rut_request.json()
            if customer['count']!=1:
                # Manda un error
                info = {
                    'error': 'El recibo no ha sido enviado, por favor contacte al personal técnico.'
                }
                return Response(info, status=status.HTTP_400_BAD_REQUEST)
            else:
                nombre = customer['results'][0]['name']
                direccion = customer['results'][0]['composite_address']
        else: # mensaje de error
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        date = request.data.get('fecha')

        info_customer = {
            "rut": rut,
            "numero": request.data.get('numero'),
            "fecha": date,
            "metodo": request.data.get('metodo'),
            "total": request.data.get('total'),
            "facturas": facturas,
            "nombre": nombre,
            "direccion": direccion
        }
        # Add webpay info if necessary
        if auth_code!=None:
            info_customer['auth_code'] = auth_code
            info_customer['tipo_pago'] = request.data.get('payment_type')
            info_customer['numero_cuotas'] = request.data.get('shares_number')
            info_customer['monto_cuotas'] = request.data.get('shares_amount')
            info_customer['tarjeta'] = request.data.get('card_number')
        
        # Create pdf
        rendered_html = html_template.render(info_customer).encode(encoding="UTF-8")
        html = HTML(string=rendered_html, base_url=request.build_absolute_uri())
        pdf = html.write_pdf(stylesheets=[css])
        pdf_filename = None
        # Save temporarily to send email
        with tempfile.NamedTemporaryFile(delete=False) as output:
            output.write(pdf)
            output.flush()
            pdf_filename = output.name
            # print(output.name)
        # Check if pdf was wrote
        if not pdf_filename:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Get token for sending email
        url = global_preferences['iris_url']+global_preferences['get_iris_auth_token']
        auth_request = requests.post(url,
            headers={'Content-Type': 'application/json'},
            json={
                'username': global_preferences['iris_username'],
                'password': global_preferences['iris_password']
            },
            verify=False)
        auth_token = ""
        if auth_request.status_code == 200:
            auth_token_response = auth_request.json()
            auth_token = auth_token_response['token']
        else: # mensaje de error
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # Send email to endpoint
        # Use auth token 
        receiver = request.data.get('email')
        message = "¡ Gracias por preferirnos!" # Para evitar problema al enviar correo desde Iris
        sender = global_preferences['iris_username']
        subject = f"Recibo de pago - Banda Ancha ({date})"
        files = {'file': (f"recibo-{date}.pdf", open(pdf_filename,'rb'))}
        url = global_preferences['iris_url']+global_preferences['send_iris_email']
        # url = 'http://httpbin.org/post' # Test para ver que se esta enviando
        email_request = requests.post(url,
            headers={
                # 'Content-Type': "multipart/form-data",
                'Authorization': 'JWT ' + auth_token
                },
            data={
                "receiver": receiver,
                "message": message,
                "sender": sender,
                "subject": subject
                },
            files=files,
            verify=False)
        # print(email_request.status_code)
        # print(email_request.json())
        if email_request.status_code != 201:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        ## Como se enviaba antes sin usar api de Iris ##
        # email_from = settings.EMAIL_HOST_USER
        # recipient_list = [request.data.get('email'),]
        # email = EmailMessage(subject, message, email_from, recipient_list)
        # email.attach(f"recibo-{date}.pdf", pdf, "application/pdf")
        ### email.content_subtype = "pdf"  # Main content is now text/html -> envia solo el pdf sin mensaje
        # email.encoding = 'utf-8'
        # email.send()
        
        return Response(info, status=status.HTTP_201_CREATED)


def generate_pdf(request):
    """ Vista de prueba para generar pdf """
    # Rendered
    html_string = render_to_string('factura_bandaancha.html')
    css = CSS(join(settings.BASE_DIR, 'staticfiles/css/style_factura_bandaancha.css'))
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    # result = html.write_pdf(stylesheets=[css, bootstrap])
    result = html.write_pdf(stylesheets=[css])

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    return response


def vars_javascript(request):
    """ Variables para usar en scripts """
    api_rut = reverse('rut_api-list', kwargs={'version':'v1'})
    api_paypal_payment = reverse('paypalpayment_api-list', kwargs={'version':'v1'})
    invoice = reverse('comprobante')
    inicio = reverse('inicio')
    recibo = reverse('recibo')
    ajax_monto = "/ajax_check/"
    set_info = "/set_values/"
    # recibo_webpay = reverse('recibo_webpay')
    send_email = reverse('send_email_api-list', kwargs={'version':'v1'})
    init_transaction = reverse('webpaytransaction_api-list', kwargs={'version':'v1'})
    # confirm_webpay_transaction = reverse('webpaytransaction_api-confirm-transaction', kwargs={'version':'v1'})
    # final_webpay_transaction = reverse('webpaytransaction_api-final-transaction', kwargs={'version':'v1'})
    api_webpay_payment = reverse('webpaypayment_api-list', kwargs={'version':'v1'})

    exchange_rate = float(Indicators.objects.filter(kind=3).distinct("kind").values("kind", "created", "value").first()["value"]) #global_preferences['usd_exchange_rate']
    usd_percentage = global_preferences['usd_percentage']
    usd_plus = global_preferences['usd_plus']
    host = HttpRequest.get_host(request)
    if ('pagos.bandaancha.cl' in host) or ('pomaire.devel7.cl' in host) or  ('localhost' in host) or ('127.0.0.1' in host):
        title="Pagos Banda Ancha" 
    else:
        title="Pagos Giga" 

    data = {
        'api_rut': api_rut,
        'api_paypal_payment': api_paypal_payment,
        'exchange_rate': exchange_rate,
        'usd_percentage': usd_percentage,
        'usd_plus':usd_plus,
        'invoice': invoice,
        'inicio': inicio,
        'receipt': recibo,
        'ajax_monto':ajax_monto,
        'set_info':set_info,
        'title':title,
        # 'webpay_receipt': recibo_webpay,
        'send_email': send_email,
        'api_webpay_transaction': init_transaction,
        # 'api_webpay_confirm_transaction': confirm_webpay_transaction,
        # 'api_webpay_final_transaction': final_webpay_transaction,
        'api_webpay_payment': api_webpay_payment
    }

    http_res = 'var Django = ' + json.dumps(data, indent=1)
    return HttpResponse(http_res, content_type="application/javascript")


def info_javascript(request):
    """ Variables para usar en scripts """
    x = uuid.UUID(request.session["uuid"])
    info=InfoMatrix.objects.get(uuid_session=x)
    data = {
            "amount": info.amount, 
            "amount_str": str(info.amount), 
            'invoices': info.invoices, 
            'debts': info.debts,
            'debt':info.debt, 
            'rut': info.customer_rut,
            'descriptions': info.description
        }
    http_res = 'var Info = ' + json.dumps(data, indent=1)
    return HttpResponse(http_res, content_type="application/javascript")

"""
Funcion que permite pasar un Decimal o datetime a un json .
"""
def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).name)


def set_monto(request, search):
    x = uuid.UUID(request.session["uuid"])
    if search != " ":
        mark_to_pay=search.split(",")
        info=InfoMatrix.objects.get(uuid_session=x)
        deudas=info.debts.split(",")
        count=0
        for i in range(len(mark_to_pay)):
            if count==0:
                count=int(deudas[i])
            else:
                count=count+int(deudas[i])
        monto=count
        info.amount=monto
        info.save()
        context = {"amount": info.amount, 
        'invoices': info.invoices, 
        'debts': info.debts,
        'debt':info.debt, 
        'rut': info.customer_rut}
    else:
        context = {"amount": 0, 
            'invoices': "", 
            'debts': "",
            'debt':0, 
            'rut': ""}

    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")

"""
def home(request):
    return render(request, "prueba.html", {})
"""

def ajax_monto(request, search):
    x = uuid.UUID(request.session["uuid"])
    if search != " ":
        mark_to_pay=search.split(",")
        info=InfoMatrix.objects.get(uuid_session=x)
        context = {"amount": info.amount, 
        'invoices': info.invoices, 
        'debts': info.debts,
        'debt':info.debt, 
        'rut': info.customer_rut}
    else:
        context = {"amount": 0, 
            'invoices': "", 
            'debts': "",
            'debt':0, 
            'rut': ""}
    data = json.dumps({"data": context}, default=default)
    return HttpResponse(data, content_type="application/json")

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)