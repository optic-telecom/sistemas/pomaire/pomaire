# Generated by Django 2.2.5 on 2020-12-28 18:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('common', '0007_auto_20200825_1533'),
    ]

    operations = [
        migrations.CreateModel(
            name='Indicators',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (4, 'Dólar acuerdo')], default=1, verbose_name='kind')),
                ('value', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='value')),
            ],
            options={
                'verbose_name': 'Indicators',
                'ordering': ['kind', '-created'],
                'permissions': (('list_indicators', 'Can list indicators'),),
            },
        ),
        migrations.CreateModel(
            name='HistoricalIndicators',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('status_field', models.BooleanField(default=True)),
                ('id_data', models.UUIDField(blank=True, db_index=True, null=True)),
                ('kind', models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (4, 'Dólar acuerdo')], default=1, verbose_name='kind')),
                ('value', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='value')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical Indicators',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
