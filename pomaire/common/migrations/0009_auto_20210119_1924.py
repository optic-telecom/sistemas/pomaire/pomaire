# Generated by Django 2.2.5 on 2021-01-19 19:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0008_historicalindicators_indicators'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalindicators',
            name='kind',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (3, 'Dólar observado'), (4, 'Dólar acuerdo')], default=1, verbose_name='kind'),
        ),
        migrations.AlterField(
            model_name='indicators',
            name='kind',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Unidad de fomento (UF)'), (3, 'Dólar observado'), (4, 'Dólar acuerdo')], default=1, verbose_name='kind'),
        ),
    ]
