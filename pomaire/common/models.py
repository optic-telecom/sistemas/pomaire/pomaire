# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from simple_history.models import HistoricalRecords

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)
    history = HistoricalRecords(inherit = True)

    class Meta:
        abstract = True


class BasePayment(BaseModel):
    """ Información general de los pagos """
    # buy_order = models.CharField('Orden de compra único generado por Pomaire', max_length=26, unique=True)
    amount = models.DecimalField('Amount', max_digits=32, decimal_places=2)
    customer_rut = models.CharField('RUT del cliente', max_length=20)
    registered_in_matrix = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']
    
    def __str__(self):
        return f"Pago {self.id} de {self.customer_rut} por {self.amount}"


class PayedInvoice(BaseModel):
    """ Detalle de facturas de pagos completados en pomaire para control """
    invoice_id = models.PositiveIntegerField()
    payment = models.ForeignKey(BasePayment, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f"{self.invoice_id}"


class InfoMatrix(BaseModel):
    """ Detalle de las Boletas de Matrix """
    uuid_session = models.UUIDField(editable = settings.DEBUG)
    customer_rut = models.CharField('RUT del cliente', max_length=20)
    amount = models.PositiveIntegerField('Amount', default=0)
    debt = models.PositiveIntegerField("Debt", default=0)
    invoices = models.TextField('Folio de Invoices')
    description = models.TextField('Descripcion de Invoices')
    debts = models.TextField('Deudas de Invoices')
    customer_name = models.TextField('Nombre del cliente', null=True, blank=True)
    services = models.TextField('Servicios de Invoices', null=True, blank=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f"Info de {self.customer_rut}"

class Indicators(BaseModel):
    UF = 1
    DOLAR = 3
    DOLAR_INTERCAMBIO = 4

    INDICATOR_CHOICES = (
        (UF, "Unidad de fomento (UF)"),
        (DOLAR, "Dólar observado"),
        (DOLAR_INTERCAMBIO, "Dólar acuerdo"),
    )
    kind = models.PositiveSmallIntegerField(
        ("kind"), default=UF, choices=INDICATOR_CHOICES
    )
    value = models.DecimalField(("value"), max_digits=20, decimal_places=2)

    def __str__(self):
        return str(self.kind)

    class Meta:
        ordering = ["kind", "-created"]
        verbose_name = ("Indicators")
        permissions = (
            ("list_indicators", "Can list indicators"),
        )
