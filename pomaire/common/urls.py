from django.urls import path, re_path
from django.conf.urls import url
from rest_framework import routers
from common.views import *

router = routers.DefaultRouter()
router.register(r'payedinvoice', PayedInvoiceViewSet, 'payedinvoice_api')
router.register(r'rut', RUTViewSet, 'rut_api')
router.register(r'payment', BasePaymentViewSet, 'payment_api')
router.register(r'send-email', SendEmailViewSet, 'send_email_api')

def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path('', InicioView.as_view(), name="inicio"),
    path('comprobante/', InvoiceView.as_view(), name="comprobante"),
    path('config_django.js', vars_javascript, name="vars_javascript"),
    path('info_django.js', info_javascript, name="info_javascript"),
    #path('home/', home, name="home"),
    path('sentry-debug/', trigger_error, name='sentry-debug'),
    url('pdf/', generate_pdf, name='generate_pdf'),
    path('recibo/', generate_invoice_pdf, name='recibo'),
    path("set_values/<str:search>/",set_monto, name='set_monto'),
    path("ajax_check/<str:search>/",ajax_monto, name='ajax_monto'),
    # path('comprobar-pago/', TransitionView.as_view(), name="transition"),
    # url('enviar-recibo/', send_email_pdf, name='send_email'),
]