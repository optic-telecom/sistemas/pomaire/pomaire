from dynamic_preferences.types import StringPreference, FloatPreference, IntegerPreference, BooleanPreference
from dynamic_preferences.registries import global_preferences_registry

## Matrix urls
@global_preferences_registry.register
class MatrixUrltUrl(StringPreference):
    name = 'matrix_url'
    default = 'https://demo-matrix.devel7.cl/'

@global_preferences_registry.register
class SearchRutUrl(StringPreference):
    name = 'search_rut_url'
    default = 'api/v1/payorruts/?rut='


@global_preferences_registry.register
class GetDebtUrl(StringPreference):
    name = 'get_debt_url'
    default = 'api/v1/invoices/?customer='


@global_preferences_registry.register
class GetCustomerUrl(StringPreference):
    name = 'get_customer_url'
    default = 'api/v1/customers/?rut='

@global_preferences_registry.register
class GetPaymentUrl(StringPreference):
    name = 'create_payment_url'
    default = 'api/v1/payments/'

@global_preferences_registry.register
class GetPaymentUrl(BooleanPreference):
    name = 'allow_registry_in_matrix'
    default = False


## Iris urls
@global_preferences_registry.register
class MatrixUrltUrl(StringPreference):
    name = 'iris_url'
    default = 'https://iris.devel7.cl/'

@global_preferences_registry.register
class GetIrisAuthToken(StringPreference):
    name = 'get_iris_auth_token'
    default = 'api/v1/user/token/auth/'


@global_preferences_registry.register
class SendIrisEmail(StringPreference):
    name = 'send_iris_email'
    default = 'api/v1/communications/email/'


@global_preferences_registry.register
class IrisUsername(StringPreference):
    name = 'iris_username'
    default = "superuser@optic.cl"


@global_preferences_registry.register
class IrisPassword(StringPreference):
    name = 'iris_password'
    default = "0dT8LM1ELx"


## Parametros y tokens
@global_preferences_registry.register
class LimitUrlParameter(StringPreference):
    name = 'limit_url_parameter'
    default = '&limit='+str(5)


@global_preferences_registry.register
class NotPaidUrlParameter(StringPreference):
    name = 'not_paid_url_parameter'
    default = '&paid_on__isnull=True'


@global_preferences_registry.register
class USDExchangeRate(FloatPreference):
    name = 'usd_exchange_rate'
    default = 727.80


@global_preferences_registry.register
class USDPercentage(FloatPreference):
    name = 'usd_percentage'
    default = 0.054

@global_preferences_registry.register
class USDPlus(FloatPreference):
    name = 'usd_plus'
    default = 0.3

@global_preferences_registry.register
class SystemToken(StringPreference):
    name = 'system_token'
    default = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'

## Paypal credentials
@global_preferences_registry.register
class PaypalMode(StringPreference):
    name = 'paypal_mode'
    default = 'sandbox'

@global_preferences_registry.register
class PaypalSandboxUrl(StringPreference):
    name = 'paypal_sandbox_url'
    default = 'https://api.sandbox.paypal.com'

@global_preferences_registry.register
class PaypalLiveUrl(StringPreference):
    name = 'paypal_live_url'
    default = 'https://api.paypal.com'

@global_preferences_registry.register
class PaypalCurrency(StringPreference):
    name = 'paypal_currency'
    default = 'USD'

@global_preferences_registry.register
class PaypalBandaClientId(StringPreference):
    name = 'paypal_banda_client_id'
    default = 'AXagWqXF_D6oiqB6IuonmsLcQcKBeeN4CYguPzrP4ILZiIHmDq98si6sZxHlFaxfVVS8g31YMER6c_TE'

@global_preferences_registry.register
class PaypalBandaClientSecret(StringPreference):
    name = 'paypal_banda_client_secret'
    default = 'EAQzh9pqbArsdZFtUS97WdSRJrIlSIdcA6YICftRqhYbW4TidZkMbd7xzOYNqYgC34UHss_lNajnCDpC'

@global_preferences_registry.register
class PaypalGigaClientId(StringPreference):
    name = 'paypal_giga_client_id'
    default = 'AXagWqXF_D6oiqB6IuonmsLcQcKBeeN4CYguPzrP4ILZiIHmDq98si6sZxHlFaxfVVS8g31YMER6c_TE'

@global_preferences_registry.register
class PaypalGigaClientSecret(StringPreference):
    name = 'paypal_giga_client_secret'
    default = 'EAQzh9pqbArsdZFtUS97WdSRJrIlSIdcA6YICftRqhYbW4TidZkMbd7xzOYNqYgC34UHss_lNajnCDpC'

## Webpay credentials

@global_preferences_registry.register
class WebpayBandaCommerceCode(IntegerPreference):
    name = 'webpay_banda_commerce_code'
    default =  597020000540

@global_preferences_registry.register
class WebpayGigaCommerceCode(IntegerPreference):
    name = 'webpay_giga_commerce_code'
    default =  597020000540
