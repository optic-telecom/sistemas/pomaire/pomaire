from rest_framework import serializers
from .models import *

class RUTSerializer(serializers.Serializer):
   """ Serializer con la información de un pago """
   rut = serializers.CharField(max_length=20)
   debt = serializers.DecimalField(max_digits=32, decimal_places=2)


class BasePaymentSerializer(serializers.ModelSerializer):
   """ Serializer de modelo base de pagos """
   class Meta:
      model = BasePayment
      fields = ('amount','customer_rut','registered_in_matrix')


class PayedInvoiceSerializer(serializers.ModelSerializer):
   """ Serializer de pagos realizados """
   class Meta:
      model = PayedInvoice
      fields = ('invoice_id', 'payment')