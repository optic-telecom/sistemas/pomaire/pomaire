# coding: utf-8
import xlsxwriter
import io, datetime, json
from django.core.management import BaseCommand
from common.models import *
from webpay.models import Payment as WebpayPayment
from datetime import date, datetime
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from decimal import *
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):

        pagoWebpay=WebpayPayment.objects.all()
        data = []

        # Valores para el excel.
        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('Fecha','Rut', 'Monto','Folios','Tipo','Cantidad de cuotas','Monto de cada cuota','Ult numeros de tarjeta','Código de autorización', 'Orden de compra' )
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 1

        for i in pagoWebpay:
            print(i.id)
            folios=PayedInvoice.objects.filter(payment=i.parent_payment)
            invoices=str(folios[0].invoice_id)
            if folios.count()>1:
                for folio in folios[1:]:
                    invoices=invoices+","+str(folio.invoice_id)
    
            if i.shares_amount:
                money=int(i.shares_amount)
            else:
                money=int(i.parent_payment.amount)
                
                
            worksheet.write(row,0,str(i.transaction_date))
            worksheet.write(row,1,str(i.parent_payment.customer_rut))
            worksheet.write(row,2,str(money))
            worksheet.write(row,3,str(invoices))
            worksheet.write(row,4,str(i.get_payment_type_display()))
            worksheet.write(row,5,str(i.shares_number))
            worksheet.write(row,6,str(i.shares_amount) if i.shares_amount else "")
            worksheet.write(row,7,str(i.last_card_number))
            worksheet.write(row,8,str(i.authorization_code))
            worksheet.write(row,9,str(i.order_id))
            row=row+1

        # Escribe los valores en el excel.
        workbook.close()
        output_file.seek(0)

        myFile = open('pagos_Webpay.xlsx', 'wb')
        with myFile:
            myFile.write(output_file.read())

        print("Listo Webpay")