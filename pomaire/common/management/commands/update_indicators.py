# coding: utf-8
import io, datetime
from django.core.management import BaseCommand
from common.models import *
from datetime import date
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from datetime import datetime
from decimal import *
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):

        # Intenta traer el ultimo valor del dolar.
        try:
            dolar = float(Indicators.objects.filter(kind=4).distinct("kind").values("kind", "created", "value").first()["value"])
        except:
            dolar=None

        # Intenta traer el ultimo valor de uf.
        try:
            uf = float(Indicators.objects.filter(kind=1).distinct("kind").values("kind", "created", "value").first()["value"])
        except:
            uf=None

        data = {
            "uf": uf,
            "dolar": dolar}
        #print(data)

        matrix_url=settings.MATRIX_URL
        #url="http://127.0.0.1:8001/api/v1/update_indicators/"
        url=matrix_url+"api/v1/update_indicators/"
        aa_request = requests.post(url,
            headers={'content-type': 'application/json','Authorization': global_preferences['system_token']},
            verify=False, data= json.dumps(data))
        print(aa_request)
        print(aa_request.text)