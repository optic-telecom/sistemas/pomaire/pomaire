# coding: utf-8
import io, datetime
from django.core.management import BaseCommand
from common.models import *
from datetime import date
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from datetime import datetime
from decimal import *
from django.conf import settings

# 1.#Importar los módulos necesarios
# Crea la conexión con el Webservice
import zeep
# Facilita el arreglo de los datos recibidos desde el Webservice
from zeep.helpers import serialize_object
# Verifica el orden correlativo de las fechas
import datetime as dt
# Permite que el programa espere un determinado período de tiempo antes de hacer una nueva consulta
from time import sleep 
# Detiene la ejecución en caso de error 
import sys

class Command(BaseCommand):
    def handle(self, *args, **options):

        # Intenta traer el ultimo valor del dolar.
        try:
            dolar = Indicators.objects.filter(kind=3).distinct("kind").values("kind", "created", "value").first()["value"]
        except:
            dolar=None

        # Intenta traer el ultimo valor de uf.
        try:
            uf = Indicators.objects.filter(kind=1).distinct("kind").values("kind", "created", "value").first()["value"]
        except:
            uf=None

        # Creación de inputs para conectar al Webservice
        user=settings.BCC_USER
        pw=settings.BCC_PASSWD

        fecha=datetime.today().strftime('%Y-%m-%d')
        #print(datetime.today().strftime('%Y-%m-%d'))
        fInic=fecha 
        fFin=fecha 

        series=["F073.TCO.PRE.Z.D", "F073.UFF.PRE.Z.D"]
        series=[x.upper() for x in series]
        #print(series)

        # Para chequear que los codigos son validos.
        for ser_cod in reversed(series):
            if ser_cod[-1] in ["D","M","T","A"]:
                pass
            else:
                print("Serie "+ser_cod+" inexistente. Chequea el código")
                series.remove(ser_cod)

        # Para eliminar los duplicados.
        series_freq=[x[-1] for x in series]
        #print(series_freq)
        series_freq=list(dict.fromkeys(series_freq))
        # series_freq será la lista que contendrá los valores únicos de frecuencia
        #print(series_freq)

        # Para obtener la frecuencia.
        for x in range(len(series_freq)):
            if series_freq[x]=="D":
                series_freq[x]=series_freq[x].replace("D","DAILY")
            elif series_freq[x]=="M":
                series_freq[x]=series_freq[x].replace("M","MONTHLY")
            elif series_freq[x]=="T":
                series_freq[x]=series_freq[x].replace("T","QUARTERLY")
            elif series_freq[x]=="A":
                series_freq[x]=series_freq[x].replace("A","ANNUAL")
            else:
                pass
        #print(series_freq)

        # Url del WebService 
        wsdl="https://si3.bcentral.cl/SieteWS/SieteWS.asmx?wsdl"
        client=zeep.Client(wsdl)

        #print(client)
        
        for frequ in series_freq:
            for attempt in range(4):
                try:
                    #Se consulta usando el usuario, password y frecuencia de interés
                    res_search=client.service.SearchSeries(user,pw,frequ)
                    #print(res_search)
                    #Se limpia la información obtenida
                    res_search=res_search["SeriesInfos"]["internetSeriesInfo"]
                    #print(res_search)
                    res_search=serialize_object(res_search)
                    #print(res_search)
                    res_search={ serie_dict['seriesId']: [serie_dict['spanishTitle'],serie_dict['frequency']] for serie_dict in res_search }
                    #print(res_search)
                    print("Frecuencia "+str(frequ)+" encontrada. Agregando")
                    break
                except Exception as e:
                    #print(str(e))
                    print("Intento "+str(attempt)+": La frecuencia"+str(frequ)+ " no fue encontrada")
                    #En caso de error, se esperan 20 segundos antes de volver a consultar la serie
                    sleep(20)
            else:
                print("Frecuencia "+str(frequ)+" no fue encontrada. Deteniendo ejecución")
                sys.exit("Deteniendo ejecución")
        
        for serieee in series:
            # Se genera un loop para hacer 10 intentos de consulta por serie. 
            # Si tiene éxito, continúa con la siguiente serie, 
            # si no tiene éxito, intenta nuevamente
            for attempt in range(4):
                try:
                    #Creación del objeto que contendrá el código de serie
                    ArrayOfString=client.get_type('ns0:ArrayOfString')
                    value=ArrayOfString(serieee)

                    result=client.service.GetSeries(user,pw,fInic,fFin, value)
                    #Se omite la serie si no hay observaciones en el período solicitado
                    if result["Series"]["fameSeries"][0]["obs"]==[]:
                        print("La serie "+str(serieee)+" no tiene observaciones para el período seleccionado")
                        break
                    #print(result)
                    obs=result["Series"]["fameSeries"][0]["obs"]
                    #print(obs)

                    if serieee=="F073.TCO.PRE.Z.D" and obs[0]['value']!=float(dolar):
                        Indicators.objects.create(kind=3,value=Decimal(obs[0]['value']))
                    if serieee=="F073.UFF.PRE.Z.D" and obs[0]['value']!=float(uf):
                        Indicators.objects.create(kind=1,value=Decimal(obs[0]['value']))

                    #Se limpia la información obtenida, dejando como nombre de fila el código de serie y, como columnas, las fechas en formato dd-mm-aaaa
                    result=serialize_object(result["Series"]["fameSeries"][0]["obs"])
                    #print(result)

                    print("Serie "+str(serieee)+" encontrada. Agregando")
                    break
                except:
                    print("Intento "+str(attempt)+": La serie "+str(serieee)+" no fue encontrada")
                    #En caso de error, se esperan 20 segundos antes de volver a consultar la serie
                    sleep(20)
            else:
                print("La serie "+str(serieee)+" no fue encontrada. Omitiendo")

        # Actualizar los valores en Matrix.

        # Intenta traer el ultimo valor del dolar.
        try:
            dolar = float(Indicators.objects.filter(kind=3).distinct("kind").values("kind", "created", "value").first()["value"])
        except:
            dolar=None

        # Intenta traer el ultimo valor de uf.
        try:
            uf = float(Indicators.objects.filter(kind=1).distinct("kind").values("kind", "created", "value").first()["value"])
        except:
            uf=None

        data = {
            "uf": uf,
            "dolar": dolar}
        #print(data)

        matrix_url=settings.MATRIX_URL
        #url="http://127.0.0.1:8001/api/v1/update_indicators/"
        url=matrix_url+"api/v1/update_indicators/"
        aa_request = requests.post(url,
            headers={'content-type': 'application/json','Authorization': global_preferences['system_token']},
            verify=False, data= json.dumps(data))
        print(aa_request)
        print(aa_request.text)