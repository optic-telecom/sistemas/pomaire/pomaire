# coding: utf-8
import xlsxwriter
import io, datetime, json
from django.core.management import BaseCommand
from common.models import *
from paypal.models import Payment as PaypalPayment
from datetime import date, datetime
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from decimal import *
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):

        pagoPaypal=PaypalPayment.objects.all()
        data = []

        # Valores para el excel.
        output_file = io.BytesIO()
        workbook = xlsxwriter.Workbook(output_file)
        worksheet = workbook.add_worksheet()

        header = ('Fecha','Rut', 'Monto','Folios','Monto USD','USD exchange rate','ID Transaccion', 'Orden de compra' )
        bold = workbook.add_format({'bold': True})
        worksheet.set_row(0, None, bold)
        worksheet.write_row(0, 0, header)
        row = 1

        for i in pagoPaypal:
            # print(i.id)
            folios=PayedInvoice.objects.filter(payment=i.parent_payment)
            invoices=str(folios[0].invoice_id)
            if folios.count()>1:
                for folio in folios[1:]:
                    invoices=invoices+","+str(folio.invoice_id)
    
            money=int(i.parent_payment.amount)

            # Guarda la info                
            worksheet.write(row,0,str(i.transaction_date))
            worksheet.write(row,1,str(i.parent_payment.customer_rut))
            worksheet.write(row,2,str(money))
            worksheet.write(row,3,str(invoices))
            worksheet.write(row,4,str(i.usd_amount))
            worksheet.write(row,5,str(i.exchange_rate))
            worksheet.write(row,6,str(i.transaction_id))
            worksheet.write(row,7,str(i.order_id))
            row=row+1

        # Escribe los valores en el excel.
        workbook.close()
        output_file.seek(0)

        myFile = open('pagos_Paypal.xlsx', 'wb')
        with myFile:
            myFile.write(output_file.read())

        print("Listo Paypal")