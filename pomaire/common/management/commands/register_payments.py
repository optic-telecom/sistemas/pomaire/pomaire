# coding: utf-8
import io, datetime
from django.core.management import BaseCommand
from common.models import *
from paypal.models import Payment as PaypalPayment
from webpay.models import Payment as WebpayPayment
from datetime import date
from common.send_request import *
from dynamic_preferences.registries import global_preferences_registry
from datetime import datetime
from decimal import *
from django.conf import settings

class Command(BaseCommand):
    def handle(self, *args, **options):
        
        pagoBase=BasePayment.objects.filter(registered_in_matrix=False)
        print("Pagos sin registrar total:")
        print(pagoBase.count())
        print("Listo")

        pagoPaypal=PaypalPayment.objects.filter(parent_payment__registered_in_matrix=False)
        print("Pagos Paypal total:")
        print(pagoPaypal.count())
        print("Listo")

        for i in pagoPaypal:
            folios=PayedInvoice.objects.filter(payment=i.parent_payment)
            invoices=str(folios[0].invoice_id)
            if folios.count()>1:
                for folio in folios[1:]:
                    invoices=invoices+","+str(folio.invoice_id)
    
            money=int(i.parent_payment.amount)

            data_to_matrix={'paid_on': i.created, 
            'deposited_on': i.created, 
            'amount': money, 
            'manual': False, 
            'kind': 13, 
            'operation_number': i.order_id, 
            'transfer_id': i.transaction_id,
            'cleared': False,
            'comment': 'Pago desde Pomaire cliente ' + str(i.parent_payment.customer_rut), 
            'customer_rut': i.parent_payment.customer_rut, 
            'invoices': invoices}
            #print(data_to_matrix)
        
            # Intenta registrar el pago en Matrix.
            if global_preferences["allow_registry_in_matrix"]:
                response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
                #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
                #print(response)
                if response.status_code == 201:
                    # Marca como registrado el pago base.
                    i.parent_payment.registered_in_matrix = True
                    i.parent_payment.save()
                else:
                    print(response)
                    print(response.text)
        
        pagoWebpay=WebpayPayment.objects.filter(parent_payment__registered_in_matrix=False)
        print("Pagos Webpay total:")
        print(pagoWebpay.count())
        print("Listo")

        
        for i in pagoWebpay:
            folios=PayedInvoice.objects.filter(payment=i.parent_payment)
            invoices=str(folios[0].invoice_id)
            if folios.count()>1:
                for folio in folios[1:]:
                    invoices=invoices+","+str(folio.invoice_id)
    
            if i.shares_amount:
                money=int(i.shares_amount)
            else:
                money=int(i.parent_payment.amount)

            data_to_matrix={'paid_on': i.created, 
            'deposited_on': i.created, 
            'amount': money, 
            'manual': False, 
            'kind': 8, 
            'operation_number': i.authorization_code, 
            'transfer_id': i.order_id,
            'cleared': False,
            'comment': 'Pago desde Pomaire cliente ' + str(i.parent_payment.customer_rut), 
            'customer_rut': i.parent_payment.customer_rut, 
            'invoices': invoices}
            #print(data_to_matrix)
        
            # Intenta registrar el pago en Matrix.
            if global_preferences["allow_registry_in_matrix"]:
                response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
                #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
                #print(response)
                if response.status_code == 201:
                    # Marca como registrado el pago base.
                    i.parent_payment.registered_in_matrix = True
                    i.parent_payment.save()
                else:
                    print(response)
                    print(response.text)

        