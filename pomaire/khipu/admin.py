from django.contrib import admin

from khipu.models import Payment
from import_export.admin import ImportExportModelAdmin
from common.admin import BaseAdmin

# Admin original
# class PaymentAdmin(admin.ModelAdmin):
#     """
#     Modelo de administracion de Ordenes de Khipu
#     """
#     list_display = (
#         "id", "payment_id", "subject", "currency", "amount",
#         "status"
#     )
#     search_fields = ["payment_id"]
#     list_per_page = 100


# admin.site.register(Payment, PaymentAdmin)

# Pomaire admin
@admin.register(Payment)
class PaymentAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id', 'payment_id', 'subject', 'currency', 'amount',
        'status')