"""pomaire URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
# from django.conf.urls import url
from khipu.urls import urlpatterns as khipu_urlpatterns
from paypal.urls import urlpatterns as paypal_urlpatterns
from paypal.urls import router as router_paypal
from common.urls import urlpatterns as  common_urlpatterns
from common.urls import router as router_common
from webpay.urls import urlpatterns as webpay_urlpatterns
from webpay.urls import router as router_webpay

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_common.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_paypal.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_webpay.urls)),
] + khipu_urlpatterns + paypal_urlpatterns + common_urlpatterns + webpay_urlpatterns
