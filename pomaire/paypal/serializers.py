from rest_framework import serializers
from common.models import BasePayment, PayedInvoice
from common.send_request import *
from .models import Payment
from dynamic_preferences.registries import global_preferences_registry
from datetime import datetime
from decimal import *
from django.conf import settings

global_preferences = global_preferences_registry.manager()

class PaymentSerializer(serializers.ModelSerializer):
    """ Serializer de pagos """
    customer_rut = serializers.CharField(write_only=True, max_length=20)
    amount = serializers.DecimalField(write_only=True, max_digits=32, decimal_places=2)
    invoices = serializers.CharField(write_only=True)

    class Meta:
        model = Payment
        fields = ('order_id', 'amount', 'usd_amount', 'exchange_rate', 'percentage',
            'usd_plus','customer_rut', 'invoices', 'transaction_date', 'transaction_id')

    def create(self, validated_data):
        """ Crea automáticamente el pago base. """
        # Calculo para ver cuanto se pago exactamente.
        usd_amount=validated_data["usd_amount"]
        percentage=validated_data["percentage"]
        usd_plus=validated_data["usd_plus"]

        comision=usd_amount*Decimal(percentage)+Decimal(usd_plus)
        real_amount=usd_amount-comision.quantize(Decimal('.01'),rounding=ROUND_DOWN)
        pesos=round(real_amount,2)*validated_data["exchange_rate"]

        # Info que se enviara a matrix 
        data_to_matrix={
            'paid_on': validated_data["transaction_date"],
            'deposited_on': validated_data["transaction_date"] ,
            'amount': int(round(pesos,2)),
            'manual':False,
            'kind': 13,
            'operation_number': validated_data["order_id"], 
            'transfer_id': validated_data["order_id"], 
            'cleared': False,
            'comment': "Pago desde Pomaire cliente "+ str(validated_data["customer_rut"]),
            'customer_rut': validated_data["customer_rut"],
            'invoices': validated_data["invoices"],
        }
        
        customer_rut = validated_data.pop('customer_rut')
        amount = validated_data.pop('amount')
        amount = int(round(pesos,2))
        invoices = validated_data.pop('invoices')
        invoices_list = invoices.split(",")

        #print(validated_data)

        print(global_preferences["create_payment_url"])
        if global_preferences["allow_registry_in_matrix"]:
            response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
            #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
            print(response)
            if response.status_code == 201:
                # Crea el pago base y se marca como registrado en matrix.
                base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount, registered_in_matrix=True)
            else:
                # Crea el pago base y se marca como no registrado en matrix.
                base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount)
        else:
            # Crea el pago base.
            base_payment = BasePayment.objects.create(customer_rut=customer_rut, amount=amount)
        # Crear invoices pagados
        for folio in invoices_list:
            invoice = PayedInvoice.objects.create(invoice_id=folio, payment=base_payment)
        # Agrega pago base a validated_data
        validated_data['parent_payment'] = base_payment
        paypal_payment = Payment.objects.create(**validated_data)
        return paypal_payment

def new():
    validated_data={
        'order_id': '26Y11741VK136424B', 
        'amount': Decimal('29970.00'), 
        'usd_amount': Decimal('13.81'), #Decimal('43.24'), 
        'exchange_rate': Decimal('782.5'), #Decimal('727.80'), 
        'percentage': Decimal('0.054'), 
        'customer_rut': '6.616.979-0', 
        'invoices': '7,1601,3359',
        'transaction_date':datetime.now()}  #datetime.datetime(2020, 8, 5, 17, 19, 48, tzinfo=<UTC>)}

    usd_amount=validated_data["usd_amount"]
    percentage=validated_data["percentage"]
    usd_plus=global_preferences["usd_plus"]

    comision=usd_amount*percentage+Decimal(usd_plus)
    print(comision.quantize(Decimal('.01'),rounding=ROUND_DOWN))
    real_amount=usd_amount-comision.quantize(Decimal('.01'),rounding=ROUND_DOWN)
    print(real_amount)
    pesos=round(real_amount,2)*validated_data["exchange_rate"]
    print(int(round(pesos,2)))

    data_to_matrix={
            'paid_on': validated_data["transaction_date"],
            'deposited_on': validated_data["transaction_date"] ,
            'amount': int(validated_data["amount"]),
            'manual':False,
            'kind': 13,
            'operation_number': validated_data["order_id"], 
            'transfer_id': validated_data["order_id"], 
            'cleared': False,
            'comment': "Pago desde Pomaire cliente "+ str(validated_data["customer_rut"]),
            'customer_rut': validated_data["customer_rut"],
            'invoices': validated_data["invoices"],
        }

    print(data_to_matrix)
    if global_preferences["allow_registry_in_matrix"]:
        response = generate_request(settings.MATRIX_URL+global_preferences["create_payment_url"], data_to_matrix)
        #response = generate_request(global_preferences['matrix_url']+global_preferences["create_payment_url"], data_to_matrix)
        print(response)
        if response.status_code == 201:
            print("Se creo")
        else:
            print("No se creo")
    else:
        print("No esta activado")

#new()