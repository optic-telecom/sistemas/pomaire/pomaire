from rest_framework import routers
from django.urls import path

from .views import PaymentViewSet

router = routers.DefaultRouter()

router.register(r'paypal/payment',PaymentViewSet, 'paypalpayment_api')

urlpatterns = [
    # path('paypal/pay/', paypal_sample, name='paypal_pay'),
]