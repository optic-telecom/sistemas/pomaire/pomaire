from django.contrib import admin
from common.admin import BaseAdmin
from import_export.admin import ImportExportModelAdmin
from .models import Payment

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()

@admin.register(Payment)
class PaymentAdmin(BaseAdmin, ImportExportModelAdmin):
    list_display = ('id','order_id','transaction_id', 'usd_amount','exchange_rate', # 'order_status','authorization_status', 'capture_status',
        'percentage', 'transaction_date')