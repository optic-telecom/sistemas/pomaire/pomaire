# Generated by Django 2.2.5 on 2020-08-24 15:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paypal', '0012_auto_20191128_2301'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalpayment',
            name='transaction_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Id de la transaccion de Paypal'),
        ),
        migrations.AddField(
            model_name='payment',
            name='transaction_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Id de la transaccion de Paypal'),
        ),
    ]
