# Generated by Django 2.2.5 on 2019-09-16 12:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('paypal', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historicalpayment',
            options={'get_latest_by': 'history_date', 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical payment'},
        ),
        migrations.AlterModelOptions(
            name='payment',
            options={'ordering': ['buy_order']},
        ),
    ]
