from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action

from .models import Payment
from .serializers import PaymentSerializer

class PaymentViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Paypal Payment
    retrieve:
        End point for Paypal Payment
    update:
        End point for Paypal Payment
    delete:
        End point for Paypal Payment
    """
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = (AllowAny,)

# def paypal_sample(request):
#     return render(request, 'paypal.html')