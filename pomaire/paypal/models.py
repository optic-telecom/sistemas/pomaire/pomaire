from django.db import models
from common.models import BasePayment, BaseModel

# CREATED = 1
# SAVED = 2
# APPROVED = 3
# VOIDED = 4
# COMPLETED = 5
# ORDER_STATUS_CHOICES = (
#     (CREATED, 'Orden creada'),
#     (SAVED, 'Guardado, sigue en proceso'),
#     (APPROVED, 'Aprobado por el cliente'),
#     (VOIDED, 'Anulado'),
#     (COMPLETED, 'Pago autorizado'),
# )

# CREATED = 1
# CAPTURED = 2
# DENIED = 3
# EXPIRED = 4
# PARTIALLY_CAPTURED = 5
# VOIDED = 6
# PENDING = 7
# AUTHORIZATION_STATUS_CHOICES = (
#     (CREATED, 'Pago creado'),
#     (CAPTURED, 'Pago con una o más capturas'),
#     (DENIED, 'Paypal no puede autorizar fondos para el pago'),
#     (EXPIRED, 'Expirado'),
#     (PARTIALLY_CAPTURED, 'Pago parcialmente capturado'),
#     (VOIDED, 'Anulado'),
#     (PENDING, 'Pendiente'),
# )

# COMPLETED = 1
# DECLINED = 2
# PARTIALLY_REFUNDED = 3
# PENDING = 4
# REFUNDED = 5
# CAPTURE_STATUS_CHOICES = (
#     (COMPLETED, 'Completado'),
#     (DECLINED, 'No capturado'),
#     (PARTIALLY_REFUNDED, 'Pago parcialmente reembolsado'),
#     (PENDING, 'Pendiente'),
#     (REFUNDED, 'Reembolso'),
# )

class Payment(BaseModel):
    """ Información de los pagos de Paypal """
    parent_payment = models.OneToOneField(BasePayment, on_delete=models.PROTECT, related_name='general_paypal_payment')
    order_id = models.CharField('Orden de compra único generado por Paypal', max_length=256)
    usd_amount = models.DecimalField('USD amount', max_digits=32, decimal_places=2)
    exchange_rate = models.DecimalField('USD exchange rate', max_digits=5, decimal_places=2)
    percentage = models.DecimalField('Added percentaje', max_digits=4, decimal_places=3)
    usd_plus = models.DecimalField('Added usd', max_digits=4, decimal_places=3, null=True, blank=True)
    # Estos se eliminan porque los pagos se capturan y autorizan automaticamente al momento de realizarse
    # order_status = models.PositiveSmallIntegerField(choices=ORDER_STATUS_CHOICES, null=True, blank=True)
    # authorization_status = models.PositiveSmallIntegerField(choices=AUTHORIZATION_STATUS_CHOICES, null=True, blank=True)
    # capture_status = models.PositiveSmallIntegerField(choices=CAPTURE_STATUS_CHOICES, null=True, blank=True)
    transaction_date = models.DateTimeField('Fecha y hora de autorización del pago', null=True, blank=True)
    transaction_id = models.CharField('Id de la transaccion de Paypal', max_length=256, null=True, blank=True)
    
    class Meta:
        ordering = ['-id']
