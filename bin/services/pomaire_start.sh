#!/bin/bash

NAME="149.28.109.212" # Name of the application
DJANGODIR=/home/pomaire/pomaire_backend/pomaire # Django project directory
LOGFILE=/home/pomaire/pomaire_backend/logs/gunicorn_pomaire.log
SOCKFILE=/home/pomaire/pomaire_backend/run/gunicorn.sock # we will communicate using this unix socket
#LOGDIR_PROJECT=/home/pulso/sentinel/logs # we will communicate using this unix socket
USER=pomaire # the user to run as
GROUP=pomaire # the group to run as
NUM_WORKERS=4 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.$1 # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=600

echo "Starting $NAME as `whoami`"

#alias python='/usr/bin/python3.6'
#alias gunicorn='/home/pulso/.local/bin/gunicorn'
source /home/pomaire/pomaire_env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

LOGDIR=$(dirname $LOGFILE)
test -d $LOGDIR || mkdir -p $LOGDIR
# Create the run directory if it doesn't exist for logs pulso
#test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/pomaire/pomaire_env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--timeout $TIMEOUT \
--capture-output \
--log-file=$LOGFILE 2>>$LOGFILE
